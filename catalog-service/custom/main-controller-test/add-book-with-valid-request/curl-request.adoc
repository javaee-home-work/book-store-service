[source,bash]
----
$ curl 'http://localhost:8080/book/add' -i -X POST \
    -H 'Content-Type: application/json' \
    -d '{"id":1,"title":"Clean Code","author":"C Martin","price":20.0,"quantity":1,"image":"https://cdn.coursehunter.net/courses/268x160/testirovanie-java-s-pomoshchyu-junit-5-i-mockito.webp"}'
----