package com.example.catalogservice.model;

import lombok.Data;

@Data
public class Product {

    private Long id;
    private String title;
    private String author;
    private String image;
    private Double price;
    private Long userId;

}