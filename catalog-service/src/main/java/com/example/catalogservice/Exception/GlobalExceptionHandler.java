package com.example.catalogservice.Exception;

import com.example.catalogservice.model.Response;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(BookNotFoundException.class)
    public Response notFoundBookByTitle(BookNotFoundException ex){
        return Response.fail().withMessage(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public Response globalHandlerException(Exception ex){
        return Response.fail().withMessage(ex.getMessage());
    }
}
