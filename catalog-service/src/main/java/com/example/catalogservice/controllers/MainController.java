package com.example.catalogservice.controllers;

import com.example.catalogservice.model.Book;
import com.example.catalogservice.model.Response;
import com.example.catalogservice.services.BookService;
import com.sun.source.tree.LambdaExpressionTree;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = "/book", produces = MediaType.APPLICATION_JSON_VALUE)
public class MainController {
    private BookService service;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Response addBook(@RequestBody @Valid Book book){
        Book bookNew = service.create(book);
        return Response.ok(bookNew);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Book getBookById(@PathVariable("id") Long id){
        Book book = service.getBook(id);
        return book;
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public Response getAllBooks(@RequestParam(required = false) String title,
                                  @RequestParam(required = false) String author,
                                  @RequestParam(required = false) Double price,
                                  @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "5") int size,
                                  @RequestParam(defaultValue = "") List<String> sortList,
                                  @RequestParam(defaultValue = "DESC") String sort
                                  ){
        Page<Book>bookPage = service.getAllBooks(title,author,price,page,size,sortList,sort);
        return Response.ok(bookPage);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response updateBookById(@PathVariable("id") Long id, @RequestBody Book book){
        Book bookNew = service.updateBook(id,book);
        return Response.ok(bookNew);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response deleteBookById(@PathVariable("id") Long id){
        service.deleteBook(id);
        return Response.ok("successful");
    }
}
