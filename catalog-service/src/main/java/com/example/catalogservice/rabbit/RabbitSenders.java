package com.example.catalogservice.rabbit;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitSenders {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendEventNewBook(String message){
        rabbitTemplate.convertAndSend("new_book","new_book",message);
    }
}
