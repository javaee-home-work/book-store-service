package com.example.catalogservice.rabbit;

import com.example.catalogservice.model.Product;
import com.example.catalogservice.services.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
@Slf4j
@Component
public class RabbitListeners {
    @Autowired
    private BookService service;

    @SneakyThrows
    @RabbitListener(queues = "minus_quantity")
    public void changeQuantity(String message){
        Product[] productsArray  = new ObjectMapper().readValue(message, Product[].class);
        List<Product> products = Arrays.asList(productsArray);
        for (int i = 0; i <products.size() ; i++) {
            service.minusQuantity(products.get(i).getTitle());
            log.info("minus Quantity {}",products.get(i).getTitle());
        }
    }
}
