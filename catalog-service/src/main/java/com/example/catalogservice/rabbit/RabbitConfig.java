package com.example.catalogservice.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    Exchange newBook() {
        return new TopicExchange("new_book", true, false);
    }

    @Bean
    Queue newBookNotification() {
        return new Queue("new_book", true, false, false);
    }

    @Bean
    Binding curtBinding(Queue orderFromCurt, Exchange curtBook){
        return new Binding(orderFromCurt.getName(), Binding.DestinationType.QUEUE, curtBook.getName(), "new_book", null);
    }
}
