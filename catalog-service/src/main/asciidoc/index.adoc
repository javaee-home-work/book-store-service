= Catalog Service Documentation

== Add book
*add book endpoint* `/book/add`

include::{snippets}/main-controller-test/add-book-with-valid-request/http-request.adoc[]
include::{snippets}/main-controller-test/add-book-with-valid-request/request-fields.adoc[]
include::{snippets}/main-controller-test/add-book-with-valid-request/request-body.adoc[]
include::{snippets}/main-controller-test/add-book-with-valid-request/http-response.adoc[]

== Get By Id
*get by id end point* `/book/{id}`

include::{snippets}/main-controller-test/test-get-book-by-id-when-id-exist/http-request.adoc[]
include::{snippets}/main-controller-test/test-get-book-by-id-when-id-exist/curl-request.adoc[]
include::{snippets}/main-controller-test/test-get-book-by-id-when-id-exist/response-body.adoc[]

== Get all Books
*get all books end-point* `/book/`

include::{snippets}/main-controller-test/test-get-all-books/http-request.adoc[]
include::{snippets}/main-controller-test/test-get-all-books/curl-request.adoc[]
include::{snippets}/main-controller-test/test-get-all-books/response-body.adoc[]

== Update book
*update book by id* `/book/{id}`

include::{snippets}/main-controller-test/update-book-by-id/http-request.adoc[]
include::{snippets}/main-controller-test/update-book-by-id/curl-request.adoc[]
include::{snippets}/main-controller-test/update-book-by-id/request-body.adoc[]
include::{snippets}/main-controller-test/update-book-by-id/response-body.adoc[]

== Delete book
*delete book by id* `/book/{id}`

include::{snippets}/main-controller-test/delete-book-by-id/http-request.adoc[]
include::{snippets}/main-controller-test/delete-book-by-id/curl-request.adoc[]
include::{snippets}/main-controller-test/delete-book-by-id/response-body.adoc[]

