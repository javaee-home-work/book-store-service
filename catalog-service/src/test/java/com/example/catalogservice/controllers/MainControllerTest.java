package com.example.catalogservice.controllers;

import com.example.catalogservice.Exception.BookNotFoundException;
import com.example.catalogservice.model.Book;
import com.example.catalogservice.services.BookService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.assertj.core.api.BDDAssumptions.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureRestDocs
@WebMvcTest(controllers = {MainControllerTest.class, ErrorController.class})
class MainControllerTest {

    private MockMvc mockMvc;
    @MockBean
    private BookService bookService;


    private final Long ID = 1L;
    private final String TITLE = "Clean Code";
    private final String AUTHOR = "C Martin";
    private final Double PRICE = 20.0;
    private final Integer QUANTITY = 1;
    private final String IMAGE = "https://cdn.coursehunter.net/courses/268x160/testirovanie-java-s-pomoshchyu-junit-5-i-mockito.webp";

    @RegisterExtension
    final RestDocumentationExtension restDocumentation = new RestDocumentationExtension ();

    @BeforeEach
    void setUp(RestDocumentationContextProvider restDocumentation) {
        MainController mainController = new MainController(bookService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(mainController)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }


    @Test
    @DisplayName("Add book with valid request")
    void addBookWithValidRequest() throws Exception {
        Book book = new Book();
        book.setId(ID);
        book.setTitle(TITLE);
        book.setAuthor(AUTHOR);
        book.setPrice(PRICE);
        book.setQuantity(QUANTITY);
        book.setImage(IMAGE);

       mockMvc.perform(
                post("/book/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(book)))
               .andDo(print())
               .andDo(document("{class-name}/{method-name}",
                       requestFields(
                               fieldWithPath("id").description("Id of book"),
                               fieldWithPath("title").description("Name of book"),
                               fieldWithPath("author").description("Author of book"),
                               fieldWithPath("price").description("Price for one book"),
                               fieldWithPath("quantity").description("Quantity books"),
                               fieldWithPath("image").description("Image of book for presentation")
                       )
                       ))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.status").value("ok"));

    }

    @Test
    @DisplayName("add book with invalid request")
    void addBookWithInvalidRequest() throws Exception {
        Book book = new Book();
        book.setId(ID);
        book.setAuthor(AUTHOR);
        book.setPrice(PRICE);
        book.setQuantity(QUANTITY);
        book.setImage(IMAGE);

         mockMvc.perform(
                        post("/book/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(book)))
                 .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void TestGetBookByIdWhenIdExist() throws Exception {
        Book book = new Book();
        book.setId(ID);
        book.setTitle(TITLE);
        book.setAuthor(AUTHOR);
        book.setPrice(PRICE);
        book.setQuantity(QUANTITY);
        book.setImage(IMAGE);

        Mockito.when(bookService.getBook(ID)).thenReturn(book);

        mockMvc.perform(
                get("/book/{id}",ID))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(ID))
                .andExpect(jsonPath("$.title").value(TITLE))
                .andExpect(jsonPath("$.author").value(AUTHOR))
                .andExpect(jsonPath("$.price").value(PRICE))
                .andExpect(jsonPath("$.quantity").value(QUANTITY))
                .andExpect(jsonPath("$.image").value(IMAGE));

    }

    @Test
    void getBookByIdWhenIdNotExist() throws Exception {
        Mockito.when(bookService.getBook(ID)).thenThrow( new BookNotFoundException("no found book with this id"+ID));

        mockMvc.perform(
                        get("/book/{id}",ID))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isNotFound());

    }

    @Test
    public void TestGetAllBooks() throws Exception {

        Book book = new Book();
        book.setId(ID);
        book.setTitle(TITLE);
        book.setAuthor(AUTHOR);
        book.setPrice(PRICE);
        book.setQuantity(QUANTITY);
        book.setImage(IMAGE);
        PageImpl<Book> bookPage = new PageImpl<>(Collections.singletonList(book));

        when(bookService.getAllBooks(null, null, null, 0, 5, Collections.emptyList(), "DESC")).thenReturn(bookPage);


        mockMvc.perform(get("/book/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data.content[0].id").value(ID))
                .andExpect(jsonPath("$.data.content[0].title").value(TITLE))
                .andExpect(jsonPath("$.data.content[0].author").value(AUTHOR))
                .andExpect(jsonPath("$.data.content[0].price").value(PRICE))
                .andExpect(jsonPath("$.data.content[0].quantity").value(QUANTITY))
                .andExpect(jsonPath("$.data.content[0].image").value(IMAGE));
    }

    @Test
    void updateBookById() throws Exception {
        Book book = new Book();
        book.setId(ID);
        book.setTitle(TITLE);
        book.setAuthor(AUTHOR);
        book.setPrice(PRICE);
        book.setQuantity(QUANTITY);
        book.setImage(IMAGE);
        Book book2 = new Book();
        book2.setId(ID);
        book2.setTitle(TITLE+"2");
        book2.setAuthor(AUTHOR+"2");
        book2.setPrice(PRICE);
        book2.setQuantity(QUANTITY);
        book2.setImage(IMAGE);

        Mockito.when(bookService.getBook(ID)).thenReturn(book);

        mockMvc.perform(
                put("/book/{id}",ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(book2)))
                .andDo(document("{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("id").description("Id of book"),
                                fieldWithPath("title").description("Name of book"),
                                fieldWithPath("author").description("Author of book"),
                                fieldWithPath("price").description("Price for one book"),
                                fieldWithPath("quantity").description("Quantity books"),
                                fieldWithPath("image").description("Image of book for presentation")
                        )
                        ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"));
    }

    @Test
    void deleteBookById() throws Exception {

        doNothing().when(bookService).deleteBook(ID);

        mockMvc.perform(
                delete("/book/{id}", ID))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data").value("successful"));
    }
}