package com.example.catalogservice.services;

import com.example.catalogservice.Exception.BookNotFoundException;
import com.example.catalogservice.model.Book;

import com.example.catalogservice.rabbit.RabbitConfig;
import com.example.catalogservice.rabbit.RabbitSenders;
import com.example.catalogservice.repository.BookRepository;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.domain.Specification;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
@SpringBootTest
class BookServiceImplTest {

    private BookServiceImpl bookService;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private RabbitSenders rabbitSenders;
    @Captor
    ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);


    @BeforeEach
    void setUp() {
        initMocks(this);
        this.bookService = new BookServiceImpl(bookRepository, rabbitSenders);
    }

    @SneakyThrows
    @Test
    @DisplayName("When we are created user we should return all field of the Object")
    void whenWeCreatedUserShouldBeReturn() {
        Book book = new Book();
        book.setId(1L);
        book.setAuthor("Test Test");
        book.setTitle("Clean Test");
        book.setPrice(200.0);
        book.setQuantity(2);

        when(bookRepository.save(any(Book.class))).thenReturn(book);


        Book created = bookService.create(book);
        assertThat(created.getId()).isSameAs(book.getId());
        assertThat(created.getAuthor()).isSameAs(book.getAuthor());
        assertThat(created.getTitle()).isSameAs(book.getTitle());
        assertThat(created.getPrice()).isSameAs(book.getPrice());
        assertThat(created.getQuantity()).isSameAs(book.getQuantity());
        verify(bookRepository).save(book);

        verify(rabbitSenders).sendEventNewBook(messageCaptor.capture());
        String messageJson = messageCaptor.getValue();
        Book newBookEvent =  new ObjectMapper().readValue(messageJson, Book.class);
        assertThat(newBookEvent.getTitle()).isEqualTo(created.getTitle());
        assertThat(newBookEvent.getImage()).isEqualTo(created.getImage());
        assertThat(newBookEvent.getAuthor()).isEqualTo(created.getAuthor());
        assertThat(newBookEvent.getQuantity()).isEqualTo(created.getQuantity());
    }

    @Test
    @DisplayName("get book by exist id")
    void WhenWeWantGetBookById() {
        Long id = 1L;
        Book book = new Book();
        book.setId(id);
        book.setAuthor("Test Test");
        book.setTitle("Clean Test");
        book.setPrice(200.0);
        book.setQuantity(2);

        when(bookRepository.findById(id)).thenReturn(Optional.of(book));

        Book expected = bookService.getBook(id);
        assertThat(expected).isSameAs(book);

    }

    @Test()
    @DisplayName("get book by dont exist id")
    void WhenWeWantGetBookByIdDontExist() {
        Long id = 1L;
        Book book = new Book();
        book.setId(id);
        book.setAuthor("Test Test");
        book.setTitle("Clean Test");
        book.setPrice(200.0);
        book.setQuantity(2);

        when(bookRepository.findById(2L)).thenThrow(BookNotFoundException.class);

        assertThrows(BookNotFoundException.class, ()->{
            bookService.getBook(id);
        },"no found book with this id");

    }

    @Test
    void TestUpdateBook() {
        Long id = 1L;
        Book book = new Book();
        book.setId(id);
        book.setAuthor("Test Test");
        book.setTitle("Clean Test");
        book.setPrice(200.0);
        book.setQuantity(2);
        Book bookNew = new Book();
        bookNew.setId(id);
        bookNew.setAuthor("Test2 Test");
        bookNew.setTitle("Clean2 Test");
        bookNew.setPrice(220.0);
        bookNew.setQuantity(1);

        when(bookRepository.findById(id)).thenReturn(Optional.of(book));
        when(bookRepository.save(bookNew)).thenReturn(bookNew);

        Book expected = bookService.updateBook(id,bookNew);
        assertThat(expected.getAuthor()).isEqualTo(bookNew.getAuthor());
        assertThat(expected.getTitle()).isEqualTo(bookNew.getTitle());
        assertThat(expected.getPrice()).isEqualTo(bookNew.getPrice());

    }

    @Test
    void testDeleteBook() {
        Long bookId = 1L;
        doNothing().when(bookRepository).deleteById(bookId);
        bookService.deleteBook(bookId);
        verify(bookRepository, times(1)).deleteById(bookId);
    }

    @Test
    void getAllBooksShouldReturnCorrectResults() {

        String title = "Clean Code";
        String author = "Robert Martin";
        Double price = 19.99;
        int page = 0;
        int size = 10;
        List<String> sortList = Arrays.asList("title", "author");
        String sortOrder = "asc";
        Book book1 = new Book();
        book1.setTitle("Clean Code");
        book1.setAuthor("Robert Martin");
        book1.setPrice(19.99);

        Book book2 = new Book();
        book2.setTitle("Code Complete");
        book2.setAuthor("Steve McConnell");
        book2.setPrice(29.99);

        List<Book> books = Arrays.asList(book1, book2);
        Page<Book> bookPage = new PageImpl<>(books);

        when(bookRepository.findAll(Mockito.any(Specification.class), Mockito.any(Pageable.class))).thenReturn(bookPage);

        Page<Book> result = bookService.getAllBooks(title, author, price, page, size, sortList, sortOrder);

        Assertions.assertEquals(2, result.getTotalElements());
        Assertions.assertEquals(book1.getTitle(), result.getContent().get(0).getTitle());
        Assertions.assertEquals(book1.getAuthor(), result.getContent().get(0).getAuthor());
        Assertions.assertEquals(book1.getPrice(), result.getContent().get(0).getPrice());
        Assertions.assertEquals(book2.getTitle(), result.getContent().get(1).getTitle());
        Assertions.assertEquals(book2.getAuthor(), result.getContent().get(1).getAuthor());
        Assertions.assertEquals(book2.getPrice(), result.getContent().get(1).getPrice());
    }
}