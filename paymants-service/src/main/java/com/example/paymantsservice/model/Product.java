package com.example.paymantsservice.model;

import jakarta.persistence.*;
import lombok.Data;
@Entity
@Table(name = "product")
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String author;
    private String image;
    private Double price;
    private Long userId;

}
