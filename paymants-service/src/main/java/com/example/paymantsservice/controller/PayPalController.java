package com.example.paymantsservice.controller;


import com.example.paymantsservice.model.OrderPay;
import com.example.paymantsservice.model.PaymentObject;
import com.example.paymantsservice.model.Product;
import com.example.paymantsservice.rabbit.RabbitSender;
import com.example.paymantsservice.service.PayPalService;
import com.example.paymantsservice.service.PaymentObjectService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@AllArgsConstructor
public class PayPalController {

    @Autowired
   private PayPalService service;

   private PaymentObjectService paymentObjectService;
   private RabbitSender rabbitSender;

    public static final String SUCCESS_URL = "pay/success";
    public static final String CANCEL_URL = "pay/cancel";

    @GetMapping("/order-pay/{id}")
    public String home(Model model, @PathVariable("id") Long id) {
        model.addAttribute("payment",paymentObjectService.getByUserId(id));
        model.addAttribute("products", paymentObjectService.getAllByUserId(id));
        return "pay";
    }

    @PostMapping("/pay")
    public String payment(@ModelAttribute("order") OrderPay orderPay) {
        Payment payment = service.createPayment(orderPay.getPrice(), orderPay.getCurrency(), orderPay.getMethod(),
                orderPay.getIntent(), orderPay.getDescription(), "http://localhost:3535/" + CANCEL_URL,
                "http://localhost:3535/" + SUCCESS_URL);
        for(Links link:payment.getLinks()) {
            if(link.getRel().equals("approval_url")) {
                return "redirect:"+link.getHref();
            }
        }
        return "redirect:/";
    }

    @GetMapping(value = CANCEL_URL)
    public String cancelPay() {
        return "cancel";
    }

    @SneakyThrows
    @GetMapping(value = SUCCESS_URL)
    public String successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId) {
        Payment payment = service.executePayment(paymentId, payerId);
        System.out.println(payment.toJSON());
        if (payment.getState().equals("approved")) {
            PaymentObject paymentObject = paymentObjectService.getByStatusId(2l);
            PaymentObject paymentObjectNew = paymentObjectService.updateStatus(paymentObject.getId());
            List<Product> products = paymentObjectService.getAllByUserId(paymentObject.getUserId());
            rabbitSender.sendSuccesfulEventToCatalog(new ObjectMapper().writeValueAsString(products));
            rabbitSender.sendSuccesfulEventToMail(new ObjectMapper().writeValueAsString(products));
            rabbitSender.sendEventToOrderWithResult(new ObjectMapper().writeValueAsString(paymentObjectNew));
            rabbitSender.sendEventToBasketWithResult(new ObjectMapper().writeValueAsString(paymentObjectNew));
            paymentObjectService.deleteProductByUserId(paymentObject.getUserId());
            return "success";
        }
        return "redirect:/";
    }


}
