package com.example.paymantsservice.service;

import com.paypal.api.payments.Payment;

public interface PayPalService {
    Payment createPayment(Double total,
                          String currency,
                          String method,
                          String intent,
                          String description,
                          String cancelUrl,
                          String successUrl
                          );

    Payment executePayment(String paymentId, String prayerId);
}
