package com.example.paymantsservice.service;

import com.example.paymantsservice.model.PaymentObject;
import com.example.paymantsservice.model.Product;
import com.example.paymantsservice.repository.PaymentRepository;
import com.example.paymantsservice.repository.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class PaymentObjectServiceImpl implements PaymentObjectService {
    private static final Long STATUS_ID = 2L;
    private final PaymentRepository repository;
    private final ProductRepository productRepository;
    private RestTemplate restTemplate;

    private final String URL_CATALOG = "lb://CATALOG-SERVICE/book/{id}";


    @Override
    public PaymentObject create(PaymentObject paymentObject) {
        paymentObject.setStatusId(2L);
        return repository.save(paymentObject);
    }

    @Override
    public PaymentObject getByUserId(Long user_id) {
        return repository.findByUserIdAndStatusId(user_id, STATUS_ID);
    }

    @Override
    public PaymentObject getById(Long id) {
        return repository.findById(id).orElseThrow(()-> new RuntimeException("Not found this PaymentObject with id:" + id));
    }

    @Override
    public PaymentObject getByTotalPriceAndStatusId(Double totalPrice, Long statusId) {
        return repository.findByTotalPriceAndStatusId(totalPrice,statusId);
    }

    @Override
    public PaymentObject updateStatus(Long id) {
        return repository.findById(id).map(
                paymentObject -> {
                    paymentObject.setStatusId(3L);
                    return repository.save(paymentObject);
                }
        ).orElseThrow(()-> new RuntimeException("Not found this PaymentObject with id:" + id));
    }

    @Override
    public String getTitle(Long id) {
        ResponseEntity<Product> responseEntity = restTemplate.getForEntity(
                URL_CATALOG,
                Product.class,
                Map.of("id", id)
        );
        return responseEntity.getBody().getTitle();
    }

    @Override
    public String getAuthor(Long id) {
        ResponseEntity<Product> responseEntity = restTemplate.getForEntity(
                URL_CATALOG,
                Product.class,
                Map.of("id", id)
        );
        return responseEntity.getBody().getAuthor();
    }

    @Override
    public String getImage(Long id) {
        ResponseEntity<Product> responseEntity = restTemplate.getForEntity(
                URL_CATALOG,
                Product.class,
                Map.of("id", id)
        );
        return responseEntity.getBody().getImage();
    }

    @Override
    public Double getPrice(Long id) {
        ResponseEntity<Product> responseEntity = restTemplate.getForEntity(
                URL_CATALOG,
                Product.class,
                Map.of("id", id)
        );
        return responseEntity.getBody().getPrice();
    }

    @Override
    public Product create(Product product) {
        return productRepository.save(product);
    }

    @Override
    public List<Product> getAllByUserId(Long id) {
        return productRepository.findByUserId(id);
    }

    @Override
    public PaymentObject getByStatusId(Long id) {
        return repository.findByStatusId(id);
    }

    @Override
    public void deletePayment(Long id) {
        repository.deleteById(id);
    }
    @Transactional
    @Override
    public void deleteProductByUserId(Long userId) {
        productRepository.deleteAllByUserId(userId);
    }

}
