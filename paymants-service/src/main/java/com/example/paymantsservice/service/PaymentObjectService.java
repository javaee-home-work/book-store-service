package com.example.paymantsservice.service;

import com.example.paymantsservice.model.PaymentObject;
import com.example.paymantsservice.model.Product;

import java.util.List;

public interface PaymentObjectService {
    PaymentObject create(PaymentObject paymentObject);
    PaymentObject getByUserId(Long user_id);
    PaymentObject getById(Long id);

    PaymentObject getByTotalPriceAndStatusId(Double totalPrice, Long statusId);
    PaymentObject updateStatus(Long id);

    String getTitle(Long id);
    String getAuthor(Long id);
    String getImage(Long id);
    Double getPrice(Long id);
    Product create(Product product);
    List<Product> getAllByUserId(Long id);

    PaymentObject getByStatusId(Long id);

    void deletePayment(Long id);
    void deleteProductByUserId (Long userId);

}
