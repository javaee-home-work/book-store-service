package com.example.paymantsservice.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    Exchange payments() {
        return new TopicExchange("payments", true, false);
    }

    @Bean
    Queue paymentsToOrders() {
        return new Queue("payments_result", true, false, false);
    }

    @Bean
    Binding curtBinding(Queue paymentsToOrders, Exchange payments){
        return new Binding(paymentsToOrders.getName(), Binding.DestinationType.QUEUE, payments.getName(), "payments_result", null);
    }


    @Bean
    Queue paymentsToBasket() {
        return new Queue("pay_result_to_basket", true, false, false);
    }

    @Bean
    Binding curtBindingBasket(Queue paymentsToBasket, Exchange payments){
        return new Binding(paymentsToBasket.getName(), Binding.DestinationType.QUEUE, payments.getName(), "pay_result_to_basket", null);
    }


    @Bean
    Queue paymentsToMail() {
        return new Queue("succesful_result_to_mail", true, false, false);
    }

    @Bean
    Binding curtBindingMail(Queue paymentsToMail, Exchange payments){
        return new Binding(paymentsToMail.getName(), Binding.DestinationType.QUEUE, payments.getName(), "succesful_result_to_mail", null);
    }

    @Bean
    Queue paymentsToCatalog() {
        return new Queue("minus_quantity", true, false, false);
    }

    @Bean
    Binding curtBindingCatalog(Queue paymentsToCatalog, Exchange payments){
        return new Binding(paymentsToCatalog.getName(), Binding.DestinationType.QUEUE, payments.getName(), "minus_quantity", null);
    }
}
