package com.example.paymantsservice.rabbit;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitSender {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendEventToOrderWithResult(String message){
        rabbitTemplate.convertAndSend("payments","payments_result", message);
    }

    public void sendEventToBasketWithResult(String message){
        rabbitTemplate.convertAndSend("payments","pay_result_to_basket", message);
    }

    public void sendSuccesfulEventToMail(String message){
        rabbitTemplate.convertAndSend("payments", "succesful_result_to_mail", message);
    }

    public void sendSuccesfulEventToCatalog(String message){
        rabbitTemplate.convertAndSend("payments", "minus_quantity", message);
    }


}
