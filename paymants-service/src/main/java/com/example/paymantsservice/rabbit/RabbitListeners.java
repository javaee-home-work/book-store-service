package com.example.paymantsservice.rabbit;

import com.example.paymantsservice.model.PaymentObject;
import com.example.paymantsservice.model.Product;
import com.example.paymantsservice.model.ProductObject;
import com.example.paymantsservice.model.Status;
import com.example.paymantsservice.service.PaymentObjectService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
@Slf4j
@Component
@AllArgsConstructor
public class RabbitListeners {

    private PaymentObjectService service;
    @SneakyThrows
    @RabbitListener(queues = "payments")
    public void newPaymentsObject(String message){
        PaymentObject paymentObject = new ObjectMapper().readValue(message, PaymentObject.class);
        service.create(paymentObject);
    }

    @SneakyThrows
    @RabbitListener(queues = "info_payments")
    public void infoProduct(String message){
        ProductObject productObject = new ObjectMapper().readValue(message, ProductObject.class);
        log.info("productlist {}",productObject);
        for (int i = 0; i <productObject.getProductList().size() ; i++) {
            Product product = new Product();
            product.setUserId(productObject.getUserId());
            product.setTitle(service.getTitle(productObject.getProductList().get(i)));
            product.setAuthor(service.getAuthor(productObject.getProductList().get(i)));
            product.setImage(service.getImage(productObject.getProductList().get(i)));
            product.setPrice(service.getPrice(productObject.getProductList().get(i)));
            service.create(product);
        }
    }
}
