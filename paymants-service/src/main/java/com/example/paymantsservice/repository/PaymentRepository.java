package com.example.paymantsservice.repository;

import com.example.paymantsservice.model.PaymentObject;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<PaymentObject, Long> {
    PaymentObject findByUserId(Long userId);
    @Transactional
    PaymentObject findByUserIdAndStatusId(Long userId, Long statusId);

    PaymentObject findByStatusId(Long id);
    PaymentObject findByTotalPriceAndStatusId(Double totalPrice, Long statusId);

}
