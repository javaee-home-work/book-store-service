package com.example.paymantsservice.repository;

import com.example.paymantsservice.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByUserId(Long id);

    void deleteAllByUserId(Long userId);
}
