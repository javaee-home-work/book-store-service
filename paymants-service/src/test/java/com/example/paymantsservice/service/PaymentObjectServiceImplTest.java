package com.example.paymantsservice.service;

import com.example.paymantsservice.model.PaymentObject;
import com.example.paymantsservice.model.Product;
import com.example.paymantsservice.repository.PaymentRepository;
import com.example.paymantsservice.repository.ProductRepository;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Column;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import static org.assertj.core.api.Assertions.assertThat;



import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class PaymentObjectServiceImplTest {
    @Mock
    private  PaymentRepository repository;
    @Mock
    private  ProductRepository productRepository;
    @Mock
    private RestTemplate restTemplate;

    private PaymentObjectServiceImpl service;

    private final Long ID = 1L;

    private final Long USER_ID = 2L;

    private final Long PRODUCT_COUNT = 3L;

    private final Double TOTAL_PRICE = 330.0;
    private final Long STATUS_ID = 2L;

    private final String TITLE = "Clean Code";
    private final String AUTHOR = "Robert m Chapel";
    private final String IMAGE = "https://test.com";

    @BeforeEach
    void setUp() {
        initMocks(this);
        this.service = new PaymentObjectServiceImpl(repository,productRepository,restTemplate);
    }


    @Test
    void TestCreate() {
        PaymentObject paymentObject = new PaymentObject();
        paymentObject.setId(ID);
        paymentObject.setUserId(USER_ID);
        paymentObject.setProductCount(PRODUCT_COUNT);
        paymentObject.setTotalPrice(TOTAL_PRICE);
        paymentObject.setStatusId(STATUS_ID);

        when(repository.save(paymentObject)).thenReturn(paymentObject);

        PaymentObject expected = service.create(paymentObject);
        assertThat(expected.getId()).isEqualTo(paymentObject.getId());
        assertThat(expected.getUserId()).isEqualTo(paymentObject.getUserId());
        assertThat(expected.getProductCount()).isEqualTo(paymentObject.getProductCount());
        assertThat(expected.getTotalPrice()).isEqualTo(paymentObject.getTotalPrice());
        assertThat(expected.getStatusId()).isEqualTo(paymentObject.getStatusId());

    }

    @Test
    void TestGetByUserId() {
        PaymentObject paymentObject = new PaymentObject();
        paymentObject.setId(ID);
        paymentObject.setUserId(USER_ID);
        paymentObject.setProductCount(PRODUCT_COUNT);
        paymentObject.setTotalPrice(TOTAL_PRICE);
        paymentObject.setStatusId(STATUS_ID);

        when(repository.findByUserIdAndStatusId(USER_ID,STATUS_ID)).thenReturn(paymentObject);

        PaymentObject expected = service.getByUserId(USER_ID);

        assertThat(expected.getId()).isEqualTo(paymentObject.getId());
        assertThat(expected.getUserId()).isEqualTo(paymentObject.getUserId());
        assertThat(expected.getProductCount()).isEqualTo(paymentObject.getProductCount());
        assertThat(expected.getTotalPrice()).isEqualTo(paymentObject.getTotalPrice());
        assertThat(expected.getStatusId()).isEqualTo(paymentObject.getStatusId());

    }

    @Test
    void TestGetById() {
        PaymentObject paymentObject = new PaymentObject();
        paymentObject.setId(ID);
        paymentObject.setUserId(USER_ID);
        paymentObject.setProductCount(PRODUCT_COUNT);
        paymentObject.setTotalPrice(TOTAL_PRICE);
        paymentObject.setStatusId(STATUS_ID);

        when(repository.findById(ID)).thenReturn(Optional.of(paymentObject));

        PaymentObject expected = service.getById(ID);

        assertThat(expected.getId()).isEqualTo(paymentObject.getId());
        assertThat(expected.getUserId()).isEqualTo(paymentObject.getUserId());
        assertThat(expected.getProductCount()).isEqualTo(paymentObject.getProductCount());
        assertThat(expected.getTotalPrice()).isEqualTo(paymentObject.getTotalPrice());
        assertThat(expected.getStatusId()).isEqualTo(paymentObject.getStatusId());
    }

    @Test
    void TestGetByTotalPriceAndStatusId() {
        PaymentObject paymentObject = new PaymentObject();
        paymentObject.setId(ID);
        paymentObject.setUserId(USER_ID);
        paymentObject.setProductCount(PRODUCT_COUNT);
        paymentObject.setTotalPrice(TOTAL_PRICE);
        paymentObject.setStatusId(STATUS_ID);

        when(repository.findByTotalPriceAndStatusId(TOTAL_PRICE,STATUS_ID)).thenReturn(paymentObject);

        PaymentObject expected = service.getByTotalPriceAndStatusId(TOTAL_PRICE,STATUS_ID);

        assertThat(expected.getId()).isEqualTo(paymentObject.getId());
        assertThat(expected.getUserId()).isEqualTo(paymentObject.getUserId());
        assertThat(expected.getProductCount()).isEqualTo(paymentObject.getProductCount());
        assertThat(expected.getTotalPrice()).isEqualTo(paymentObject.getTotalPrice());
        assertThat(expected.getStatusId()).isEqualTo(paymentObject.getStatusId());
    }

    @Test
    void TestUpdateStatus() {
        PaymentObject paymentObject = new PaymentObject();
        paymentObject.setId(ID);
        paymentObject.setUserId(USER_ID);
        paymentObject.setProductCount(PRODUCT_COUNT);
        paymentObject.setTotalPrice(TOTAL_PRICE);
        paymentObject.setStatusId(STATUS_ID);

        when(repository.findById(ID)).thenReturn(Optional.of(paymentObject));
        when(repository.save(paymentObject)).thenReturn(paymentObject);

        PaymentObject expected = service.updateStatus(ID);
        assertThat(expected.getId()).isEqualTo(paymentObject.getId());
        assertThat(expected.getUserId()).isEqualTo(paymentObject.getUserId());
        assertThat(expected.getProductCount()).isEqualTo(paymentObject.getProductCount());
        assertThat(expected.getTotalPrice()).isEqualTo(paymentObject.getTotalPrice());
        assertThat(expected.getStatusId()).isEqualTo(paymentObject.getStatusId());
        verify(repository,times(1)).save(paymentObject);
        verify(repository, times(1)).findById(ID);
    }

    @Test
    void TestGetTitle() {

    }

    @Test
    void TestGetAuthor() {
    }

    @Test
    void getImage() {
    }

    @Test
    void getPrice() {
    }

    @Test
    void testCreateProduct() {
        Product product = new Product();
        product.setId(ID);
        product.setTitle(TITLE);
        product.setAuthor(AUTHOR);
        product.setImage(IMAGE);
        product.setPrice(TOTAL_PRICE);
        product.setUserId(USER_ID);

        when(productRepository.save(product)).thenReturn(product);

        Product expected = service.create(product);
        assertThat(expected.getId()).isEqualTo(product.getId());
        assertThat(expected.getUserId()).isEqualTo(product.getUserId());
        assertThat(expected.getTitle()).isEqualTo(product.getTitle());
        assertThat(expected.getAuthor()).isEqualTo(product.getAuthor());
        assertThat(expected.getImage()).isEqualTo(product.getImage());
        assertThat(expected.getPrice()).isEqualTo(product.getPrice());
    }

    @Test
    void TestGetAllByUserId() {
        List<Product> productList = new ArrayList<>();
        Product product = new Product();
        product.setId(ID);
        product.setTitle(TITLE);
        product.setAuthor(AUTHOR);
        product.setImage(IMAGE);
        product.setPrice(TOTAL_PRICE);
        product.setUserId(USER_ID);

        Product product2 = new Product();
        product2.setId(ID+1);
        product2.setTitle(TITLE+"s");
        product2.setAuthor(AUTHOR+"s");
        product2.setImage(IMAGE+"s");
        product2.setPrice(TOTAL_PRICE+2);
        product2.setUserId(USER_ID);

        Product product3 = new Product();
        product3.setId(ID+2);
        product3.setTitle(TITLE+"a");
        product3.setAuthor(AUTHOR+"a");
        product3.setImage(IMAGE+"a");
        product3.setPrice(TOTAL_PRICE+3);
        product3.setUserId(USER_ID);
        productList.add(product);
        productList.add(product2);
        productList.add(product3);

        when(productRepository.findByUserId(USER_ID)).thenReturn(productList);

        List<Product> expected = service.getAllByUserId(USER_ID);
        assertThat(expected.size()).isEqualTo(productList.size());
        assertThat(expected.get(0).getUserId()).isEqualTo(productList.get(0).getUserId())
                        .isEqualTo(expected.get(1).getUserId()).isEqualTo(productList.get(1).getUserId());
        assertThat(expected.get(0).getId()).isEqualTo(product.getId());
        assertThat(expected.get(0).getTitle()).isEqualTo(product.getTitle());
        assertThat(expected.get(0).getAuthor()).isEqualTo(product.getAuthor());
        assertThat(expected.get(0).getImage()).isEqualTo(product.getImage());
        assertThat(expected.get(0).getPrice()).isEqualTo(product.getPrice());
        assertThat(expected.get(1).getId()).isEqualTo(product2.getId());
        assertThat(expected.get(1).getTitle()).isEqualTo(product2.getTitle());
        assertThat(expected.get(1).getAuthor()).isEqualTo(product2.getAuthor());
        assertThat(expected.get(1).getImage()).isEqualTo(product2.getImage());
        assertThat(expected.get(1).getPrice()).isEqualTo(product2.getPrice());
        assertThat(expected.get(2).getId()).isEqualTo(product3.getId());
        assertThat(expected.get(2).getTitle()).isEqualTo(product3.getTitle());
        assertThat(expected.get(2).getAuthor()).isEqualTo(product3.getAuthor());
        assertThat(expected.get(2).getImage()).isEqualTo(product3.getImage());
        assertThat(expected.get(2).getPrice()).isEqualTo(product3.getPrice());


    }

    @Test
    void TestGetByStatusId() {
        PaymentObject paymentObject = new PaymentObject();
        paymentObject.setId(ID);
        paymentObject.setUserId(USER_ID);
        paymentObject.setProductCount(PRODUCT_COUNT);
        paymentObject.setTotalPrice(TOTAL_PRICE);
        paymentObject.setStatusId(STATUS_ID);

        when(repository.findByStatusId(STATUS_ID)).thenReturn(paymentObject);

        PaymentObject expected = service.getByStatusId(STATUS_ID);

        assertThat(expected.getId()).isEqualTo(paymentObject.getId());
        assertThat(expected.getUserId()).isEqualTo(paymentObject.getUserId());
        assertThat(expected.getProductCount()).isEqualTo(paymentObject.getProductCount());
        assertThat(expected.getTotalPrice()).isEqualTo(paymentObject.getTotalPrice());
        assertThat(expected.getStatusId()).isEqualTo(paymentObject.getStatusId());
        verify(repository,times(1)).findByStatusId(STATUS_ID);
    }

    @Test
    void TestDeletePayment() {
        doNothing().when(repository).deleteById(ID);
        service.deletePayment(ID);
        verify(repository,times(1)).deleteById(ID);
    }

    @Test
    void TestDeleteProductByUserId() {
        doNothing().when(productRepository).deleteAllByUserId(USER_ID);
        service.deleteProductByUserId(USER_ID);
        verify(productRepository,times(1)).deleteAllByUserId(USER_ID);
    }
}