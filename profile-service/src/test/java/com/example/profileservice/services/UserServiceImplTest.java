package com.example.profileservice.services;

import com.example.profileservice.model.User;
import com.example.profileservice.rabbit.RabbitSender;
import com.example.profileservice.repository.UserRepository;
import com.example.profileservice.util.Exception.UserNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;


import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
@SpringBootTest
class UserServiceImplTest {
    @MockBean
    private UserRepository repository;
    @MockBean
    private RabbitSender rabbitSender;
    @Captor
    ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

    @InjectMocks
    private UserServiceImpl service;

    private final Long ID = 1l;
    private final String FIRST_NAME = "Igor";
    private final String LAST_NAME = "Popovich";
    private final String EMAIL = "popovich@gmail.com";


    @BeforeEach
    void setUp() {
        initMocks(this);
        this.service = new UserServiceImpl(repository, rabbitSender);
    }

    @Test
    void TestActivateUser() {
        String code = UUID.randomUUID().toString();
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setStatus(true);
        user.setActivationCode(code);
        Mockito.when(repository.findByActivationCode(code)).thenReturn(user);

        Boolean excepted = service.activateUser(code);
        assertThat(excepted).isEqualTo(true);
    }

    @Test
    void TestSetActivationCode() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        Mockito.when(repository.findById(ID)).thenReturn(Optional.of(user));
        Mockito.when(repository.save(user)).thenReturn(user);

        User expected = service.setActivationCode(ID);
        assertThat(expected.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user.getEmail());
        assertThat(expected.getActivationCode()).isNotEmpty();
    }

    @Test
    void TestSetDataCreatedByUser() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        Mockito.when(repository.findById(ID)).thenReturn(Optional.of(user));
        Mockito.when(repository.save(user)).thenReturn(user);

        User excepted = service.setDataCreatedByUser(ID);
        assertThat(excepted.getCreateAt()).isNotEqualTo(new Date());
    }

    @Test
    void TestGetUserByExistId() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        Mockito.when(repository.findById(ID)).thenReturn(Optional.of(user));

        User expected = service.getUserById(ID);
        assertThat(expected.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user.getEmail());
    }

    @Test
    void TestGetUserByDontExistId() {

        Mockito.when(repository.findById(ID)).thenThrow(UserNotFoundException.class);

        assertThrows(UserNotFoundException.class, () -> {
            service.getUserById(ID);
        }, "Not found this user with this id" + ID);
    }
    @SneakyThrows
    @Test
    void TestCreate() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        Mockito.when(repository.save(user)).thenReturn(user);
        Mockito.when(repository.findById(ID)).thenReturn(Optional.of(user));


        User expected = service.create(user);
        verify(rabbitSender).sendInfoUser(messageCaptor.capture());
        verify(repository,times(3)).save(user);
        String messageJson = messageCaptor.getValue();
        User rabbitUser = new ObjectMapper().readValue(messageJson, User.class);

        assertThat(rabbitUser.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(rabbitUser.getLastName()).isEqualTo(user.getLastName());
        assertThat(rabbitUser.getEmail()).isEqualTo(user.getEmail());

        assertThat(expected.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user.getEmail());


    }

    @Test
    void TestUpdateUserById() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        User user2 = new User();
        user2.setId(ID);
        user2.setFirstName(FIRST_NAME);
        user2.setLastName(LAST_NAME);
        user2.setEmail(EMAIL);

        Mockito.when(repository.findById(ID)).thenReturn(Optional.of(user));
        Mockito.when(repository.save(user)).thenReturn(user);

        User expected = service.updateUserById(ID, user2);
        assertThat(expected.getFirstName()).isEqualTo(user2.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user2.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user2.getEmail());
    }

    @Test
    void TestDeleteUserById() {
        doNothing().when(repository).deleteById(ID);
        service.deleteUserById(ID);
        verify(repository, times(1)).deleteById(ID);

    }
    @SneakyThrows
    @Test
    void TestDeactivateMailSender() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setEmailRequest(true);

        Mockito.when(repository.findById(ID)).thenReturn(Optional.of(user));
        Mockito.when(repository.save(user)).thenReturn(user);


        User expected = service.deactivateMailSender(ID);

        verify(rabbitSender).sendDeactivateMailRequest(messageCaptor.capture());
        String messageJson = messageCaptor.getValue();
        User rabbitUser = new ObjectMapper().readValue(messageJson, User.class);

        assertThat(rabbitUser.getEmailRequest()).isEqualTo(false);
        assertThat(rabbitUser.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(rabbitUser.getLastName()).isEqualTo(user.getLastName());
        assertThat(rabbitUser.getEmail()).isEqualTo(user.getEmail());

        assertThat(expected.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user.getEmail());
        assertThat(expected.getEmailRequest()).isEqualTo(false);

    }

    @Test
    void TestGetAllUsers() {
        Date createAt = new Date();

        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        User user2 = new User();
        user2.setId(ID);
        user2.setFirstName(FIRST_NAME + "2");
        user2.setLastName(LAST_NAME + "2");
        user2.setEmail(EMAIL);
        User user3 = new User();
        user3.setId(ID);
        user3.setFirstName(FIRST_NAME + "3");
        user3.setLastName(LAST_NAME + "3");
        user3.setEmail(EMAIL);
        int page = 0;
        int size = 10;
        List<String> sortList = Arrays.asList("firstName", "LastName");
        String sortOrder = "asc";

        List<User> users = List.of(user, user2, user3);
        Page<User> userPage = new PageImpl<>(users);
        when(repository.findAll(Mockito.any(Specification.class), Mockito.any(Pageable.class))).thenReturn(userPage);
        Page<User> expected = service.getAllUsers(FIRST_NAME, LAST_NAME, EMAIL, true, createAt, true, page, size, sortList, sortOrder);

        Assertions.assertEquals(3, expected.getTotalElements());
        Assertions.assertEquals(user.getFirstName(),expected.getContent().get(0).getFirstName());
        Assertions.assertEquals(user.getLastName(),expected.getContent().get(0).getLastName());
        Assertions.assertEquals(user.getEmail(),expected.getContent().get(0).getEmail());
        Assertions.assertEquals(user2.getFirstName(),expected.getContent().get(1).getFirstName());
        Assertions.assertEquals(user2.getLastName(),expected.getContent().get(1).getLastName());
        Assertions.assertEquals(user2.getEmail(),expected.getContent().get(1).getEmail());
        Assertions.assertEquals(user3.getFirstName(),expected.getContent().get(2).getFirstName());
        Assertions.assertEquals(user3.getLastName(),expected.getContent().get(2).getLastName());
        Assertions.assertEquals(user3.getEmail(),expected.getContent().get(2).getEmail());
    }
}