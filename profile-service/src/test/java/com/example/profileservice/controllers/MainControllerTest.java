package com.example.profileservice.controllers;

import aj.org.objectweb.asm.TypeReference;
import com.example.profileservice.dto.UserDto;
import com.example.profileservice.model.User;
import com.example.profileservice.rabbit.RabbitSender;
import com.example.profileservice.services.UserService;
import com.example.profileservice.util.UserMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.assertj.core.api.FactoryBasedNavigableListAssert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.jupiter.api.Assertions.*;
@WebMvcTest(controllers = {MainControllerTest.class, ErrorController.class})
@AutoConfigureRestDocs
class MainControllerTest {

    private MockMvc mockMvc;
    @MockBean
    private UserService service;
    @MockBean
    private UserMapper mapper;
    private final Long ID = 1l;
    private final String FIRST_NAME = "Igor";
    private final String LAST_NAME = "Popovich";
    private final String EMAIL = "popovich@gmail.com";

    @RegisterExtension
    final RestDocumentationExtension restDocumentation = new RestDocumentationExtension ();
    @BeforeEach
    void setUp(RestDocumentationContextProvider restDocumentation) {
        MockitoAnnotations.initMocks(this);
        MainController mainController = new MainController(service,mapper);
        this.mockMvc = MockMvcBuilders.standaloneSetup(mainController)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }
    @SneakyThrows
    @Test
    public void TestGetAllPageUsers() throws Exception {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setEmailRequest(true);

        List<User> users = Arrays.asList(user);
        Pageable pageable = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(users, pageable, users.size());
        when(service.getAllUsers(null, null, null, null, null, null, 0, 10, new ArrayList<>(), "DESC")).thenReturn(userPage);

        UserDto userDto = new UserDto();
        userDto.setId(ID);
        userDto.setFirstName(FIRST_NAME);
        userDto.setLastName(LAST_NAME);
        userDto.setEmail(EMAIL);
        userDto.setEmailRequest(true);
        List<UserDto> userDtos = Arrays.asList(userDto);
        when(mapper.toUserDtoList(users)).thenReturn(userDtos);

        mockMvc.perform(get("/user/")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(document("{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("content.[].id").description("User's id"),
                                fieldWithPath("content.[].firstName").description("User's first name"),
                                fieldWithPath("content.[].lastName").description("User's last name"),
                                fieldWithPath("content.[].email").description("User's email"),
                                fieldWithPath("content.[].emailRequest").description("User's email request"),
                                fieldWithPath("content.[].createAt").description("User's create at date"),
                                fieldWithPath("content.[].status").description("User's status"),
                                fieldWithPath("pageable.sort.empty").description("Whether the page is sorted or not"),
                                fieldWithPath("pageable.sort.sorted").description("Whether the page is sorted in ascending or descending order"),
                                fieldWithPath("pageable.sort.unsorted").description("Whether the page is sorted in any order or not sorted at all"),
                                fieldWithPath("pageable.offset").description("The offset to the first element in the page"),
                                fieldWithPath("pageable.pageNumber").description("The current page number"),
                                fieldWithPath("pageable.pageSize").description("The maximum number of elements that can be returned in one page"),
                                fieldWithPath("pageable.paged").description("Whether the page is pageable or not"),
                                fieldWithPath("pageable.unpaged").description("Whether the page is unpaged or not"),
                                fieldWithPath("last").description("Whether this is the last page or not"),
                                fieldWithPath("totalPages").description("The total number of pages available"),
                                fieldWithPath("totalElements").description("The total number of elements available in all pages"),
                                fieldWithPath("size").description("The current page size"),
                                fieldWithPath("number").description("The current page number"),
                                fieldWithPath("sort.empty").description("Whether the page is sorted or not"),
                                fieldWithPath("sort.sorted").description("Whether the page is sorted in ascending or descending order"),
                                fieldWithPath("sort.unsorted").description("Whether the page is sorted in any order or not sorted at all"),
                                fieldWithPath("first").description("Whether this is the first page or not"),
                                fieldWithPath("numberOfElements").description("The number of elements currently on this page"),
                                fieldWithPath("empty").description("Whether the current page is empty or not")
                        )

                ))
                .andExpect(status().isOk());
    }

    @Test
    void TestDeactivateMailSender() throws Exception {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setEmailRequest(true);

        Mockito.when(service.getUserById(ID)).thenReturn(user);
        Mockito.when(service.create(user)).thenReturn(user);

        mockMvc.perform(
                patch("/user/{id}",ID))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"));
    }

    @Test
    void TestCreate() throws Exception {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setCreateAt(new Date());
        user.setStatus(false);
        user.setActivationCode(UUID.randomUUID().toString());


        mockMvc.perform(
                post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(user)))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("id").description("Id of User"),
                                fieldWithPath("firstName").description("Name of User"),
                                fieldWithPath("lastName").description("Last name  of User"),
                                fieldWithPath("email").description("Email of User"),
                                fieldWithPath("emailRequest").description("user permission to send him emails: yes(true)/ no(false)"),
                                fieldWithPath("createAt").description("Date create User"),
                                fieldWithPath("status").description("This field check that user active or not (user active when he confirm registration on his mail box)"),
                                fieldWithPath("activationCode").description("This random code send to user mail box")
                        )
                ))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void TestGetById() throws Exception {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        Mockito.when(service.getUserById(ID)).thenReturn(user);

        mockMvc.perform(
                get("/user/{id}",ID))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk());


    }

    @Test
    void TestSuccessfulActivate() throws Exception {
        String code = UUID.randomUUID().toString();
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setActivationCode(code);
        Mockito.when(service.activateUser(code)).thenReturn(true);
        Mockito.when(service.getUserById(ID)).thenReturn(user);


        mockMvc.perform(
                get("/user/activate/{code}",code))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data").value("successful"));
    }

    @Test
    void TestNotSuccessfulActivate() throws Exception {
        String code = UUID.randomUUID().toString();
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setActivationCode(code);
        Mockito.when(service.activateUser(code)).thenReturn(false);
        Mockito.when(service.getUserById(ID)).thenReturn(user);


        mockMvc.perform(
                        get("/user/activate/{code}",code))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("fail"))
                .andExpect(jsonPath("$.message").value("Wrong Activate Code"));
    }

    @Test
    void TestUpdateUserById() throws Exception {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setCreateAt(new Date());
        user.setStatus(false);
        user.setActivationCode(null);
        User user2 = new User();
        user2.setId(ID);
        user2.setFirstName(FIRST_NAME+"s");
        user2.setLastName(LAST_NAME+"f");
        user2.setEmail(EMAIL);
        user.setCreateAt(new Date());
        user.setStatus(false);
        user.setActivationCode(null);

        Mockito.when(service.getUserById(ID)).thenReturn(user);
        Mockito.when(service.create(user2)).thenReturn(user2);

        mockMvc.perform(
                put("/user/{id}",ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(user2)))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("id").description("Id of User"),
                                fieldWithPath("firstName").description("Name of User"),
                                fieldWithPath("lastName").description("Last name  of User"),
                                fieldWithPath("email").description("Email of User"),
                                fieldWithPath("emailRequest").description("user permission to send him emails: yes(true)/ no(false)"),
                                fieldWithPath("createAt").description("Date create User"),
                                fieldWithPath("status").description("This field check that user active or not (user active when he confirm registration on his mail box)"),
                                fieldWithPath("activationCode").description("This random code send to user mail box")
                        )
                ))
                .andExpect(status().isOk());

    }

    @Test
    void deleteUserById() throws Exception {
        doNothing().when(service).deleteUserById(ID);

        mockMvc.perform(delete("/user/{id}",ID))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk());
    }
}