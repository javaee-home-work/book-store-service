== Profile-service

=== Rest Api

*create user endpoint* `/user/create`

include::{snippets}/main-controller-test/test-create/http-request.adoc[]
include::{snippets}/main-controller-test/test-create/request-body.adoc[]
include::{snippets}/main-controller-test/test-create/request-fields.adoc[]
include::{snippets}/main-controller-test/test-create/curl-request.adoc[]
include::{snippets}/main-controller-test/test-create/http-response.adoc[]

*Activate User* `/user/activate/{code}`

include::{snippets}/main-controller-test/test-successful-activate/curl-request.adoc[]
include::{snippets}/main-controller-test/test-successful-activate/http-request.adoc[]
include::{snippets}/main-controller-test/test-successful-activate/http-response.adoc[]

*Get All Users* `/user/`

include::{snippets}/main-controller-test/test-fet-all-page-users/curl-request.adoc[]
include::{snippets}/main-controller-test/test-fet-all-page-users/http-request.adoc[]
include::{snippets}/main-controller-test/test-fet-all-page-users/response-body.adoc[]
include::{snippets}/main-controller-test/test-fet-all-page-users/response-fields.adoc[]

*Deactivate MailRequest for User* `/user/{id}`

include::{snippets}/main-controller-test/test-deactivate-mail-sender/curl-request.adoc[]
include::{snippets}/main-controller-test/test-deactivate-mail-sender/http-request.adoc[]
include::{snippets}/main-controller-test/test-deactivate-mail-sender/http-response.adoc[]

*Get User By Id* `/user/{id}`

include::{snippets}/main-controller-test/test-get-by-id/curl-request.adoc[]
include::{snippets}/main-controller-test/test-get-by-id/http-request.adoc[]
include::{snippets}/main-controller-test/test-get-by-id/http-response.adoc[]

*Update user By ID* `/user/{id}`

include::{snippets}/main-controller-test/test-update-user-by-id/curl-request.adoc[]
include::{snippets}/main-controller-test/test-update-user-by-id/http-request.adoc[]
include::{snippets}/main-controller-test/test-update-user-by-id/request-fields.adoc[]
include::{snippets}/main-controller-test/test-update-user-by-id/http-response.adoc[]
