package com.example.profileservice.util.Exception;

import com.example.profileservice.model.Response;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ClobalHandlerException {

    @ExceptionHandler(UserNotFoundException.class)
    public Response notFoundUser(UserNotFoundException ex){
        return Response.fail().withMessage(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public Response globalHandlerException(Exception ex){
        return Response.fail().withMessage(ex.getMessage());
    }
}
