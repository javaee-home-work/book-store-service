package com.example.profileservice.util;

import com.example.profileservice.dto.UserDto;
import com.example.profileservice.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserMapperImpl implements UserMapper {
    @Override
    public UserDto toDto(User user) {
        if(user == null){
            return null;
        }
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setCreateAt(user.getCreateAt());
        userDto.setStatus(user.getStatus());
        userDto.setEmailRequest(user.getEmailRequest());
        return userDto;
    }

    @Override
    public User toUser(UserDto userDto) {
        if(userDto == null){
            return null;
        }
        User user = new User();
        user.setId(userDto.getId());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setStatus(userDto.getStatus());
        user.setEmailRequest(userDto.getEmailRequest());
        return user;
    }

    @Override
    public List<UserDto> toUserDtoList(List<User> users) {
        if(users == null){
            return null;
        }
        return users.stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public List<User> toUserList(List<UserDto> userDtos) {
        if(userDtos == null){
            return null;
        }
        return userDtos.stream().map(this::toUser).collect(Collectors.toList());
    }
}
