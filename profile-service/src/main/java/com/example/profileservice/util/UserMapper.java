package com.example.profileservice.util;

import com.example.profileservice.dto.UserDto;
import com.example.profileservice.model.User;


import java.util.List;

public interface UserMapper {
    UserDto toDto(User user);

    User toUser(UserDto userDto);
    List<UserDto> toUserDtoList(List<User> users);

    List<User> toUserList(List<UserDto> userDtos);

}
