package com.example.profileservice.services;

import com.example.profileservice.model.User;
import com.example.profileservice.rabbit.RabbitSender;
import com.example.profileservice.repository.UserRepository;
import com.example.profileservice.util.Exception.UserNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.criteria.Predicate;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository repository;
    private RabbitSender rabbitSender;

    @Override
    public Boolean activateUser(String code) {
        User user = repository.findByActivationCode(code);
        if (user == null) {
            return false;
        } else {
            user.setStatus(true);
            user.setActivationCode(null);
            repository.save(user);
            return true;
        }
    }


    @Override
    public User setActivationCode(Long id) {
       return repository.findById(id).map(
                user -> {
                    user.setActivationCode(UUID.randomUUID().toString());
                    return repository.save(user);
                }
        ).orElseThrow(() -> new UserNotFoundException("Not found this user with this id"+id));
    }

    @Override
    public User setDataCreatedByUser(Long id) {
       return repository.findById(id).map(
                user -> {
                    user.setCreateAt(new Date());
                    return repository.save(user);
                }
        ).orElseThrow(() -> new UserNotFoundException("Not found this user with this id"+id));
    }

    @Override
    public User getUserById(Long id) {
        return repository.findById(id).orElseThrow(() -> new UserNotFoundException("Not found this user with this id"+id));
    }

    @SneakyThrows
    @Override
    public User create(User user) {
       User userCreate = setDataCreatedByUser(repository.save(user).getId());
       User resultUser = setActivationCode(userCreate.getId());
       rabbitSender.sendInfoUser(new ObjectMapper().writeValueAsString(resultUser));
       return resultUser;
    }

    @Override
    public User updateUserById(Long id, User user) {
        return repository.findById(id).map(
                user1 -> {
                    user1.setFirstName(user.getFirstName());
                    user1.setLastName(user.getLastName());
                    return repository.save(user1);
                }
        ).orElseThrow(() -> new UserNotFoundException("Not found this user with this id"+id));
    }

    @Override
    public void deleteUserById(Long id) {
        repository.deleteById(id);
    }

    @SneakyThrows
    @Override
    public User deactivateMailSender(Long id) {
        User userNew =  repository.findById(id).map(
                user -> {
                    user.setEmailRequest(false);
                    return repository.save(user);
                }
        ).orElseThrow(()-> new UserNotFoundException("User not found with this id: " +id));
        rabbitSender.sendDeactivateMailRequest(new ObjectMapper().writeValueAsString(userNew));
        return userNew;
    }

    @Override
    public Page<User> getAllUsers(String firstName, String lastName, String email, Boolean emailRequest, Date createAt, Boolean status, int page, int size, List<String> sortList, String sortOrder) {
        Pageable pageable = PageRequest.of(page,size,Sort.by(createSortOrder(sortList,sortOrder)));
        Page<User> userPage = repository.findAll(getUserSpecification(firstName, lastName, email, emailRequest, createAt, status),pageable);
        return userPage;
    }

    private Specification<User> getUserSpecification(final String firstName, final String lastName, final String email, final Boolean emailRequest, final Date createAt, final Boolean status){
        return ((root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.and();
            if(firstName!= null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("fisrtName"),"%"+firstName+"%"));
            if(lastName!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("lastName"),"%"+lastName+"%"));
            if(email!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("email"), "%"+email+"%"));
            if(emailRequest!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("emailRequest"),emailRequest));
            if(status!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("status"), status));
            return predicate;
        });
    }

    private List<Sort.Order> createSortOrder(List<String> sortList, String sortDirection) {
        List<Sort.Order> sorts = new ArrayList<>();
        Sort.Direction direction;
        for (String sort : sortList) {
            if (sortDirection != null) {
                direction = Sort.Direction.fromString(sortDirection);
            } else {
                direction = Sort.Direction.DESC;
            }
            sorts.add(new Sort.Order(direction, sort));
        }
        return sorts;
    }
}
