package com.example.profileservice.services;

import com.example.profileservice.model.User;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

public interface UserService {
    Boolean activateUser(String code);
    User setActivationCode(Long id);
    User setDataCreatedByUser(Long id);
    User getUserById(Long id);
    User create(User user);
    User updateUserById(Long id, User user);
    void deleteUserById(Long id);

    User deactivateMailSender(Long id);
    Page<User> getAllUsers(String firstName,
                           String lastName,
                           String email,
                           Boolean emailRequest,
                           Date createAt,
                           Boolean status,
                           int page,
                           int size,
                           List<String> sortList,
                           String sortOrder);

}
