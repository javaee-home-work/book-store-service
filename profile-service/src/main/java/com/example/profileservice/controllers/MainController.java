package com.example.profileservice.controllers;

import com.example.profileservice.dto.UserDto;
import com.example.profileservice.model.Response;
import com.example.profileservice.model.User;

import com.example.profileservice.services.UserService;

import com.example.profileservice.util.UserMapper;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class MainController {

    private UserService service;
    private UserMapper userMapper;


    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public Page<UserDto> getAllUser(@RequestParam(required = false) String firstName,
                                 @RequestParam(required = false)String lastName,
                                 @RequestParam(required = false) String email,
                                 @RequestParam(required = false) Boolean emailRequest,
                                 @RequestParam(required = false) Date createAt,
                                 @RequestParam(required = false) Boolean status,
                                 @RequestParam(defaultValue = "0") int page,
                                 @RequestParam(defaultValue = "10")int size,
                                 @RequestParam(defaultValue = "") List<String> sortList,
                                 @RequestParam(defaultValue = "DESC") String sort){

        Page<User> userPage = service.getAllUsers(firstName, lastName, email, emailRequest, createAt, status, page, size, sortList, sort);
        Page<UserDto> userDtoPage = new PageImpl<>(
                userMapper.toUserDtoList(userPage.getContent()),
                userPage.getPageable(),
                userPage.getTotalElements()
        );
        return userDtoPage;
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response deactivateMailSender(@PathVariable("id") Long id){
        User user = service .deactivateMailSender(id);
        UserDto result = userMapper.toDto(user);
        return Response.ok(result);
    }

    @SneakyThrows
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto create(@RequestBody @Valid UserDto userDto){
        User user = userMapper.toUser(userDto);
        User userNew = service.create(user);
        return userMapper.toDto(userNew);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getById(@PathVariable("id") Long id){
       User user = service.getUserById(id);
       return userMapper.toDto(user);
    }

    @GetMapping("activate/{code}")
    public Response activate(@PathVariable("code") String code){
        Boolean isActive = service.activateUser(code);
        if(isActive){
            return Response.ok("successful");
        }else {
            return Response.fail().withMessage("Wrong Activate Code");
        }
    }
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response updateUserById(@PathVariable("id") Long id, @RequestBody @Valid UserDto userDto){
        User user = service.updateUserById(id, userMapper.toUser(userDto));
        return Response.ok(userMapper.toDto(user));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response deleteUserById(@PathVariable("id") Long id){
        service.deleteUserById(id);
       return Response.ok("Successful");
    }
}
