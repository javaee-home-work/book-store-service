package com.example.profileservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class ViewController {

    @GetMapping("user/activation/{code}")
    public String activate(){
        return "activate";
    }
}
