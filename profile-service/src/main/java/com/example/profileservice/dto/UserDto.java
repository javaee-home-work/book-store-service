package com.example.profileservice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Column;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Date;

@Data
public class UserDto {
    private Long id;
    @Pattern(message = "Incorrect first name: ${validatedValue}",
            regexp = "^[A-Z][a-z]*(\\s(([a-z]{1,3})|(([a-z]+\\')?[A-Z][a-z]*)))*$")
    @Size(min = 2,max = 20, message = "Name should be between 2 and 20 characters")
    private String firstName;
    @Pattern(message = "Incorrect last name: ${validatedValue}",
            regexp = "^[A-Z][a-z]*(\\s(([a-z]{1,3})|(([a-z]+\\')?[A-Z][a-z]*)))*$")
    @NotNull
    @Size(min = 2,max = 20, message = "LastName should be between 2 and 20 characters")
    private String lastName;

    @Email
    private String email;

    private Boolean emailRequest = true;
    private Boolean status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createAt;

}
