package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.MailReport;
import com.example.mailnotificationservice.model.User;
import com.example.mailnotificationservice.repository.UserRepository;
import jakarta.persistence.Column;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.autoconfigure.security.SecurityProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.*;

class UserServiceImplTest {
    @Mock
    private UserRepository repository;

    private UserServiceImpl service;
    private final Long ID = 1l;
    private final String FIRST_NAME = "Igor";
    private final String LAST_NAME = "Popovich";
    private final String EMAIL = "popovich@gmail.com";

    @BeforeEach
    void setUp(){
        initMocks(this);
        service = new UserServiceImpl(repository);
    }
    @Test
    void TestSaved() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        when(repository.save(user)).thenReturn(user);

        User expected = service.saved(user);
        assertThat(expected.getId()).isEqualTo(user.getId());
        assertThat(expected.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user.getEmail());
        verify(repository,times(1)).save(user);
    }

    @Test
    void TestUpdateUser() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        User user2 = new User();
        user2.setId(ID);
        user2.setFirstName(FIRST_NAME+"s");
        user2.setLastName(LAST_NAME+"s");
        user2.setEmail(EMAIL);

        when(repository.findById(ID)).thenReturn(Optional.of(user));
        when(repository.save(user2)).thenReturn(user2);

        User expected = service.updateUser(ID,user2);
        assertThat(expected.getId()).isEqualTo(user2.getId());
        assertThat(expected.getFirstName()).isEqualTo(user2.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user2.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user2.getEmail());
        verify(repository,times(1)).findById(ID);
        verify(repository,times(1)).save(user2);
    }

    @Test
    void TestDeactivateMailRequest() {
        doNothing().when(repository).deleteById(ID);
        service.deactivateMailRequest(ID);
        verify(repository,times(1)).deleteById(ID);
    }

    @Test
    void TestGetById() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        when(repository.findById(ID)).thenReturn(Optional.of(user));

        User expected = service.getById(ID);

        assertThat(expected.getId()).isEqualTo(user.getId());
        assertThat(expected.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(expected.getLastName()).isEqualTo(user.getLastName());
        assertThat(expected.getEmail()).isEqualTo(user.getEmail());
        verify(repository,times(1)).findById(ID);
    }

    @Test
    void TestGetAllUsers() {
        List<User> listUsers = new ArrayList<>();
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);

        User user2 = new User();
        user2.setId(ID);
        user2.setFirstName(FIRST_NAME+"s");
        user2.setLastName(LAST_NAME+"s");
        user2.setEmail(EMAIL+"s");
        listUsers.add(user);
        listUsers.add(user2);

        when(repository.findAll()).thenReturn(listUsers);

        List<User> expected = service.getAllUsers();
        assertThat(expected.size()).isEqualTo(listUsers.size());
        assertThat(expected.get(0).getId()).isEqualTo(user.getId());
        assertThat(expected.get(0).getFirstName()).isEqualTo(user.getFirstName());
        assertThat(expected.get(0).getLastName()).isEqualTo(user.getLastName());
        assertThat(expected.get(0).getEmail()).isEqualTo(user.getEmail());
        assertThat(expected.get(1).getId()).isEqualTo(user2.getId());
        assertThat(expected.get(1).getFirstName()).isEqualTo(user2.getFirstName());
        assertThat(expected.get(1).getLastName()).isEqualTo(user2.getLastName());
        assertThat(expected.get(1).getEmail()).isEqualTo(user2.getEmail());

    }
}