package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.MailReport;
import com.example.mailnotificationservice.model.User;
import com.example.mailnotificationservice.repository.MailReportRepository;
import jakarta.persistence.Column;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.*;

class MailReportServiceImplTest {
    @Mock
    private MailReportRepository repository;
    @Mock
    private RestTemplate restTemplate;

    private MailReportServiceImpl service;

    private final Long ID = 1L;
    private static final String PROFILE_URL = "lb://PROFILE-SERVICE/user/{id}";
    private final String EVENT_NAME = "Test";
    private final String FIRST_NAME = "Igor";
    private final String LAST_NAME = "Popovich";

    @BeforeEach
    void setUp(){
        initMocks(this);
        this.service = new MailReportServiceImpl(repository,restTemplate);
    }

    @Test
    void TestCreate() {
        MailReport mailReport = new MailReport();
        mailReport.setId(ID);
        mailReport.setEventName(EVENT_NAME);
        when(repository.save(mailReport)).thenReturn(mailReport);

        MailReport expected = service.create(mailReport);
        assertThat(expected.getId()).isEqualTo(mailReport.getId());
        assertThat(expected.getEventName()).isEqualTo(mailReport.getEventName());
        verify(repository,times(1)).save(mailReport);
    }

    @Test
    void TestSetToUser() {
        MailReport mailReport = new MailReport();
        mailReport.setId(ID);
        mailReport.setEventName(EVENT_NAME);

        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);

        when(repository.findById(ID)).thenReturn(Optional.of(mailReport));
        when(repository.save(mailReport)).thenReturn(mailReport);

        MailReport expected = service.setToUser(ID,user);
        assertThat(expected.getUser()).isEqualTo(mailReport.getUser());
        assertThat(expected.getId()).isEqualTo(mailReport.getId());
        assertThat(expected.getEventName()).isEqualTo(mailReport.getEventName());
        verify(repository,times(1)).findById(ID);
        verify(repository,times(1)).save(mailReport);
    }

    @Test
    void TestGetById() {
        MailReport mailReport = new MailReport();
        mailReport.setId(ID);
        mailReport.setEventName(EVENT_NAME);

        when(repository.findById(ID)).thenReturn(Optional.of(mailReport));

        MailReport expected = service.getById(ID);
        assertThat(expected.getId()).isEqualTo(mailReport.getId());
        assertThat(expected.getEventName()).isEqualTo(mailReport.getEventName());
        verify(repository,times(1)).findById(ID);
    }

    @Test
    void TestGetAllReport() {
        MailReport mailReport = new MailReport();
        mailReport.setId(ID);
        mailReport.setEventName(EVENT_NAME);

        MailReport mailReport2 = new MailReport();
        mailReport.setId(ID+1);
        mailReport.setEventName(EVENT_NAME+"s");
        List<MailReport> mailReports = new ArrayList<>();
        mailReports.add(mailReport);
        mailReports.add(mailReport2);

        when(repository.findAll()).thenReturn(mailReports);

        List<MailReport> expected = service.getAllReport();
        assertThat(expected.size()).isEqualTo(mailReports.size());
        assertThat(expected.get(0).getId()).isEqualTo(mailReport.getId());
        assertThat(expected.get(0).getEventName()).isEqualTo(mailReport.getEventName());
        assertThat(expected.get(1).getId()).isEqualTo(mailReport2.getId());
        assertThat(expected.get(1).getEventName()).isEqualTo(mailReport2.getEventName());
    }

    @Test
    void TestGetAllMailReportFromUserId() {
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);

        MailReport mailReport = new MailReport();
        mailReport.setId(ID);
        mailReport.setEventName(EVENT_NAME);
        mailReport.setUser(user);

        MailReport mailReport2 = new MailReport();
        mailReport.setId(ID+1);
        mailReport.setEventName(EVENT_NAME+"s");
        mailReport2.setUser(user);
        List<MailReport> mailReports = new ArrayList<>();
        mailReports.add(mailReport);
        mailReports.add(mailReport2);

        when(repository.findAllByUser(user)).thenReturn(mailReports);

        List<MailReport> expected = service.getAllMailReportFromUserId(user);
        assertThat(expected.size()).isEqualTo(mailReports.size());
        assertThat(expected.get(0).getId()).isEqualTo(mailReport.getId());
        assertThat(expected.get(0).getEventName()).isEqualTo(mailReport.getEventName());
        assertThat(expected.get(1).getId()).isEqualTo(mailReport2.getId());
        assertThat(expected.get(1).getEventName()).isEqualTo(mailReport2.getEventName());
        assertThat(expected.get(0).getUser()).isSameAs(user);
        assertThat(expected.get(1).getUser()).isSameAs(user);
        verify(repository,times(1)).findAllByUser(user);
    }

    @Test
    void TestGetEmailByUserId() {
        String email = "test@gmail.com";
        User user = new User();
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(email);

        ResponseEntity<User> responseEntity = new ResponseEntity<>(user, HttpStatus.OK);
        when(restTemplate.getForEntity(PROFILE_URL,User.class, Map.of("id",ID))).thenReturn(responseEntity);

        String expected = service.getEmailByUserId(ID);
        assertThat(expected).isEqualTo(email);
    }
}