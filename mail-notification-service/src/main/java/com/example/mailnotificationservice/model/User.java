package com.example.mailnotificationservice.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "users_mail")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column(name = "email_request")
    private Boolean emailRequest;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createAt = new Date();

    private Boolean status;
    @Column(name = "activation_code")
    private String activationCode;
    @JsonManagedReference
    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.EAGER
    )
    private List<MailReport> mailReport;
}
