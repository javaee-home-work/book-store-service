package com.example.mailnotificationservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
public class Book {

    private Long id;
    private String title;
    private String author;
    private Double price;
    private Integer quantity;
    private String image;
}
