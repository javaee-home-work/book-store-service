package com.example.mailnotificationservice.rabbit;

import com.example.mailnotificationservice.model.Book;
import com.example.mailnotificationservice.model.MailReport;
import com.example.mailnotificationservice.model.Product;
import com.example.mailnotificationservice.model.User;
import com.example.mailnotificationservice.service.EmailSenderService;
import com.example.mailnotificationservice.service.MailReportService;
import com.example.mailnotificationservice.service.MailSender;
import com.example.mailnotificationservice.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Comment;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.print.attribute.standard.MediaSize;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
public class RabbitListeners {
    private EmailSenderService emailSenderService;
    private UserService userService;
    private MailReportService mailReportService;


    @SneakyThrows
    @RabbitListener(queues = "mail_sender_user")
    public void infoNewUsers(String message) {
        User user = new ObjectMapper().readValue(message, User.class);
        log.info("User {}", user);
        String text = String.format(
                "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <title>Activate Your Account</title>\n" +
                        "    <style>\n" +
                        "        body {\n" +
                        "            margin: 0;\n" +
                        "            padding: 0;\n" +
                        "            background-color: #f2f2f2;\n" +
                        "            font-family: Arial, sans-serif;\n" +
                        "        }\n" +
                        "        \n" +
                        "        .container {\n" +
                        "            max-width: 600px;\n" +
                        "            margin: 50px auto;\n" +
                        "            background-color: #fff;\n" +
                        "            box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);\n" +
                        "            padding: 30px;\n" +
                        "            text-align: center;\n" +
                        "        }\n" +
                        "        \n" +
                        "        h1 {\n" +
                        "            margin: 0 0 20px;\n" +
                        "            font-size: 36px;\n" +
                        "            color: #1abc9c;\n" +
                        "        }\n" +
                        "        \n" +
                        "        p {\n" +
                        "            margin: 0 0 20px;\n" +
                        "            font-size: 18px;\n" +
                        "            color: #555;\n" +
                        "            line-height: 1.5;\n" +
                        "        }\n" +
                        "        \n" +
                        "        .btn {\n" +
                        "            display: inline-block;\n" +
                        "            background-color: #1abc9c;\n" +
                        "            color: #fff;\n" +
                        "            text-decoration: none;\n" +
                        "            padding: 10px 20px;\n" +
                        "            font-size: 16px;\n" +
                        "            border-radius: 5px;\n" +
                        "            transition: background-color 0.2s;\n" +
                        "        }\n" +
                        "        \n" +
                        "        .btn:hover {\n" +
                        "            background-color: #148f77;\n" +
                        "        }\n" +
                        "        \n" +
                        "        .link {\n" +
                        "            display: inline-block;\n" +
                        "            color: #1abc9c;\n" +
                        "            text-decoration: none;\n" +
                        "            border-bottom: 1px solid #1abc9c;\n" +
                        "            transition: border-color 0.2s;\n" +
                        "        }\n" +
                        "        \n" +
                        "        .link:hover {\n" +
                        "            border-color: #148f77;\n" +
                        "        }\n" +
                        "    </style>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "    <div class=\"container\">\n" +
                        "        <h1>Activate Your Account</h1>\n" +
                        "        <p>Hello,</p>\n" +
                        "        <p>Thank you  %s  %s for creating an account! Please click on the button below to activate your account:</p>\n" +
                        "        <a href=\"http://localhost:8585/user/activation/%s\" class=\"btn\">Activate Account</a>\n" +
                        "        <p>If you did not create an account, please ignore this email.</p>\n" +
                        "    </div>\n" +
                        "</body>\n" +
                        "</html>\n", user.getFirstName(), user.getLastName(), user.getActivationCode()
        );
        emailSenderService.sendEmail(user.getEmail(), "Activate Code", text);
        userService.saved(user);
        MailReport mailReport = new MailReport();
        mailReport.setEventName("Activate Message");
        MailReport mailReportNew = mailReportService.create(mailReport);
        mailReportService.setToUser(mailReportNew.getId(), user);
    }

    @SneakyThrows
    @RabbitListener(queues = "new_book")
    public void infoNewBook(String message) {
        Book book = new ObjectMapper().readValue(message, Book.class);
        log.info("Book is {}", book);
        String text = String.format(
                "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "  <head>\n" +
                        "    <title>Новый товар в магазине!</title>\n" +
                        "    <style>\n" +
                        "      body {\n" +
                        "        font-family: Arial, sans-serif;\n" +
                        "        font-size: 16px;\n" +
                        "        color: #333;\n" +
                        "        line-height: 1.5;\n" +
                        "        background-color: #f4f4f4;\n" +
                        "        margin: 0;\n" +
                        "        padding: 0;\n" +
                        "      }\n" +
                        "      .container {\n" +
                        "        max-width: 800px;\n" +
                        "        margin: 0 auto;\n" +
                        "        padding: 20px;\n" +
                        "      }\n" +
                        "      h1 {\n" +
                        "        margin-top: 0;\n" +
                        "      }\n" +
                        "      .book {\n" +
                        "        display: flex;\n" +
                        "        align-items: center;\n" +
                        "        margin-bottom: 30px;\n" +
                        "      }\n" +
                        "      .book img {\n" +
                        "        max-width: 200px;\n" +
                        "        margin-right: 20px;\n" +
                        "      }\n" +
                        "      .book-info {\n" +
                        "        flex-grow: 1;\n" +
                        "      }\n" +
                        "      .book-title {\n" +
                        "        font-size: 24px;\n" +
                        "        font-weight: bold;\n" +
                        "        margin-bottom: 10px;\n" +
                        "      }\n" +
                        "      .book-author {\n" +
                        "        font-size: 18px;\n" +
                        "        margin-bottom: 10px;\n" +
                        "      }\n" +
                        "      .book-price {\n" +
                        "        font-size: 18px;\n" +
                        "        font-weight: bold;\n" +
                        "      }\n" +
                        "      .button {\n" +
                        "        display: inline-block;\n" +
                        "        padding: 10px 20px;\n" +
                        "        background-color: #4caf50;\n" +
                        "        color: #fff;\n" +
                        "        text-decoration: none;\n" +
                        "        font-size: 18px;\n" +
                        "        border-radius: 5px;\n" +
                        "        margin-top: 20px;\n" +
                        "      }\n" +
                        "      .button:hover {\n" +
                        "        background-color: #3e8e41;\n" +
                        "      }\n" +
                        "    </style>\n" +
                        "  </head>\n" +
                        "  <body>\n" +
                        "    <div class=\"container\">\n" +
                        "      <h1>New Book on the Store!!</h1>\n" +
                        "      <div class=\"book\">\n" +
                        "        <img src=\"" + book.getImage() + "\" alt=\"Книга\">\n" +
                        "        <div class=\"book-info\">\n" +
                        "          <div class=\"book-title\">" + book.getTitle() + "</div>\n" +
                        "          <div class=\"book-author\">" + book.getAuthor() + "</div>\n" +
                        "          <div class=\"book-price\">" + book.getPrice() + "$ </div>\n" +
                        "          <a href=\"https://localhost:8585/book/" + book.getId() + "\" class=\"button\">Check this Book</a>\n" +
                        "        </div>\n" +
                        "      </div>\n" +
                        "    </div>\n" +
                        "  </body>\n" +
                        "</html>\n"
        );

        List<User> emailList = userService.getAllUsers();
        for (int i = 0; i < emailList.size(); i++) {
            if (emailList.get(i).getEmailRequest()) {
                emailSenderService.sendEmail(emailList.get(i).getEmail(), "New Book!", text);
                MailReport mailReport = new MailReport();
                mailReport.setEventName("New Book");
                MailReport mailReportNew = mailReportService.create(mailReport);
                mailReportService.setToUser(mailReportNew.getId(), userService.getById(emailList.get(i).getId()));
            }
        }
    }

    @SneakyThrows
    @RabbitListener(queues = "deactivate_mail")
    public void deactivateMailRequest(String message) {
        User user = new ObjectMapper().readValue(message, User.class);
        userService.updateUser(user.getId(), user);
    }

    @SneakyThrows
    @RabbitListener(queues = "succesful_result_to_mail")
    public void sendMailSuccesfulAfterPay(String message) {
        Product[] productsArray  = new ObjectMapper().readValue(message, Product[].class);
        List<Product> products = Arrays.asList(productsArray);
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<!DOCTYPE html>\n");
        htmlBuilder.append("<html>\n");
        htmlBuilder.append("  <head>\n");
        htmlBuilder.append("    <meta charset=\"utf-8\" />\n");
        htmlBuilder.append("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n");
        htmlBuilder.append("    <title>Order Check</title>\n");
        htmlBuilder.append("    <style>\n");
        htmlBuilder.append("      body {\n");
        htmlBuilder.append("        font-family: Arial, sans-serif;\n");
        htmlBuilder.append("        background-color: #f6f6f6;\n");
        htmlBuilder.append("        color: #444444;\n");
        htmlBuilder.append("        margin: 0;\n");
        htmlBuilder.append("        padding: 0;\n");
        htmlBuilder.append("      }\n");
        htmlBuilder.append("      .container {\n");
        htmlBuilder.append("        max-width: 800px;\n");
        htmlBuilder.append("        margin: 0 auto;\n");
        htmlBuilder.append("        padding: 20px;\n");
        htmlBuilder.append("        background-color: #ffffff;\n");
        htmlBuilder.append("        box-shadow: 0px 0px 10px #cccccc;\n");
        htmlBuilder.append("      }\n");
        htmlBuilder.append("      h1 {\n");
        htmlBuilder.append("        font-size: 28px;\n");
        htmlBuilder.append("        margin-top: 0;\n");
        htmlBuilder.append("      }\n");
        htmlBuilder.append("      .order {\n");
        htmlBuilder.append("        border-collapse: collapse;\n");
        htmlBuilder.append("        width: 100%;\n");
        htmlBuilder.append("        margin-top: 20px;\n");
        htmlBuilder.append("      }\n");
        htmlBuilder.append("      .order td,\n");
        htmlBuilder.append("      .order th {\n");
        htmlBuilder.append("        border: 1px solid #dddddd;\n");
        htmlBuilder.append("        text-align: left;\n");
        htmlBuilder.append("        padding: 8px;\n");
        htmlBuilder.append("      }\n");
        htmlBuilder.append("      .order th {\n");
        htmlBuilder.append("        background-color: #f2f2f2;\n");
        htmlBuilder.append("      }\n");
        htmlBuilder.append("    </style>\n");
        htmlBuilder.append("  </head>\n");
        htmlBuilder.append("  <body>\n")
                .append("    <div>\n")
                .append("      <h1>Thank you for purchase in our Book Store!</h1>\n")
                .append("      <p>Your Order:</p>\n");
        for (Product product : products) {
            htmlBuilder.append("          <tr>\n")
                    .append("            <div>").append(product.getTitle()).append("</div>\n")
                    .append("            <div>").append(product.getAuthor()).append("</div>\n")
                    .append("            <div><img src=\"").append(product.getImage())
                    .append("\" width=\"150\" height=\"150\" alt=\"Image\"></div>\n")
                    .append("            <div>").append(product.getPrice()).append("</div>\n");
        }
        htmlBuilder.append("    </div>\n")
                .append("  </body>\n")
                .append("</html>\n");

        String html = htmlBuilder.toString();
        String email = mailReportService.getEmailByUserId(products.get(0).getUserId());
        emailSenderService.sendEmail(email,"Succesful Pay",html);

    }

}
