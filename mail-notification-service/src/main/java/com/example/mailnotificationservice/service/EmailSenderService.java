package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.User;

import java.util.List;

public interface EmailSenderService {

    void sendEmail(String to, String subject,String message);

}
