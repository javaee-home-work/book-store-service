package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.MailReport;
import com.example.mailnotificationservice.model.User;
import com.example.mailnotificationservice.repository.MailReportRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class MailReportServiceImpl implements MailReportService {

    private static final String PROFILE_URL = "lb://PROFILE-SERVICE/user/{id}";
    private MailReportRepository repository;
    private RestTemplate restTemplate;

    @Override
    public MailReport create(MailReport mailReport) {
        return repository.save(mailReport);
    }

    @Override
    public MailReport setToUser(Long id, User user) {
        return repository.findById(id).map(
                mail->{
                    mail.setUser(user);
                    return repository.save(mail);
                }
        ).orElseThrow(()-> new RuntimeException("MailReport not found with this id: "+id));
    }

    @Override
    public MailReport getById(Long id) {
        return repository.findById(id).orElseThrow(()-> new RuntimeException("MailReport not found with this id: "+id));
    }

    @Override
    public List<MailReport> getAllReport() {
        return repository.findAll();
    }

    @Override
    public List<MailReport> getAllMailReportFromUserId(User user) {
        return repository.findAllByUser(user);
    }

    @Override
    public String getEmailByUserId(Long id) {
        ResponseEntity<User> responseEntity = restTemplate.getForEntity(
                PROFILE_URL,
                User.class,
                Map.of("id",id)
        );
        return responseEntity.getBody().getEmail();
    }


}
