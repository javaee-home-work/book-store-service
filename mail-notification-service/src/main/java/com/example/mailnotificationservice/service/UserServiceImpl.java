package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.User;
import com.example.mailnotificationservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository repository;
    @Override
    public User saved(User user) {
        return repository.save(user);
    }

    @Override
    public User updateUser(Long id, User user) {
        return repository.findById(id).map(
                user1 -> {
                    user1.setFirstName(user.getFirstName());
                    user1.setLastName(user.getLastName());
                    user1.setEmail(user.getEmail());
                    user1.setEmailRequest(user.getEmailRequest());
                    user1.setStatus(user.getStatus());
                    return repository.save(user1);
                }
        ).orElseThrow(()-> new RuntimeException("User not foun with this id: "+ id));
    }

    @Override
    public void deactivateMailRequest(Long id) {
        repository.deleteById(id);
    }

    @Override
    public User getById(Long id) {
        return repository.findById(id).orElseThrow(()-> new RuntimeException("User not found with this id: "+id));
    }

    @Override
    public List<User> getAllUsers() {
        return repository.findAll();
    }
}
