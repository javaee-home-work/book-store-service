package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.User;
import com.example.mailnotificationservice.repository.MailReportRepository;
import com.example.mailnotificationservice.repository.UserRepository;
import jakarta.mail.internet.MimeMessage;
import lombok.AllArgsConstructor;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class MailSender implements EmailSenderService{


    private JavaMailSender mailSender;


    @SneakyThrows
    public void sendEmail(String emailTo, String subject, String message){
        MimeMessage message1 = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message1,true);
        helper.setTo(emailTo);
        helper.setSubject(subject);
        helper.setText(message,true);
        mailSender.send(message1);
    }




}
