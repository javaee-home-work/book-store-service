package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.MailReport;
import com.example.mailnotificationservice.model.User;

import java.util.List;

public interface MailReportService {

    MailReport create(MailReport mailReport);
    MailReport setToUser(Long id, User user);
    MailReport getById(Long id);
    List<MailReport> getAllReport();
    List<MailReport> getAllMailReportFromUserId(User user);

    String getEmailByUserId(Long id);

}
