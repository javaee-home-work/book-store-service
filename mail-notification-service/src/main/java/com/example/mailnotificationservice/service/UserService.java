package com.example.mailnotificationservice.service;

import com.example.mailnotificationservice.model.User;

import java.util.List;

public interface UserService {
    User saved(User user);
    User updateUser(Long id, User user);
    void deactivateMailRequest(Long id);
    User getById(Long id);
    List<User> getAllUsers();
}
