package com.example.mailnotificationservice.repository;

import com.example.mailnotificationservice.model.MailReport;
import com.example.mailnotificationservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MailReportRepository extends JpaRepository<MailReport, Long>  {
    List<MailReport> findAllByUser(User user);
}
