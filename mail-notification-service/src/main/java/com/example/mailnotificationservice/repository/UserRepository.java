package com.example.mailnotificationservice.repository;

import com.example.mailnotificationservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
