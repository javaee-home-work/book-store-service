package com.example.orderservice.model;

import lombok.Data;

import java.util.List;

@Data
public class ProductObject {
    private Long userId;
    private List<Long> productList;
}
