package com.example.orderservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.OneToOne;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class PaymentObject {
    private Long id;
    private Long userId;
    private Long productCount;
    private Double totalPrice;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payDate;
    private Long statusId;
}
