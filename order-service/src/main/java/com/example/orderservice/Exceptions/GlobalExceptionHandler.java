package com.example.orderservice.Exceptions;

import com.example.orderservice.model.Response;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundOrderException.class)
    public Response notfoundOrderById(NotFoundOrderException ex){
        return Response.fail().withMessage(ex.message);
    }
    @ExceptionHandler(NotFoundUserException.class)
    public Response notFoundUserById(NotFoundUserException ex){
        return Response.fail().withMessage(ex.message);
    }
}
