package com.example.orderservice.services;

import com.example.orderservice.Exceptions.NotFoundOrderException;
import com.example.orderservice.Exceptions.NotFoundUserException;
import com.example.orderservice.model.Order;
import com.example.orderservice.model.PaymentObject;
import com.example.orderservice.model.ProductObject;
import com.example.orderservice.rabbit.RabbitSender;
import com.example.orderservice.repository.OrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.persistence.criteria.Predicate;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;
    private final RabbitSender rabbitSender;

    @Override
    public Order create(Order order) {
        return repository.save(order);
    }

    @Override
    public Order getOrder(Long id) {
        return repository.findById(id).orElseThrow(()-> new NotFoundOrderException("Order not found with this id "+id));
    }

    @Override
    public Page<Order> getAllOrders(Long userId,
                                    Long productId,
                                    Integer quantity,
                                    Double priceSum,
                                    Date createAt,
                                    Boolean status,
                                    int page,
                                    int size,
                                    List<String> sortList,
                                    String sortOrder) {
        Pageable pageable = PageRequest.of(page,size,Sort.by(createSortOrder(sortList,sortOrder)));
        return repository.findAll(getSpecification(userId, productId, quantity, priceSum, createAt, status),pageable);
    }

    private Specification<Order> getSpecification(final Long userId,
                                                  final Long productId,
                                                  final Integer quantity,
                                                  final Double price,
                                                  final Date createAt,
                                                  final Boolean status
                                                  ){
        return (((root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.and();
            if(userId!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("userId"), userId));
            if(productId!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("productId"), productId));
            if(quantity!= null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("quantity"),quantity));
            if(price!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("priceSum"), price));
            if(createAt!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("createAt"), createAt));
            if(status!=null) predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("status"), status));
            return predicate;
        }));
    }

    private List<Sort.Order> createSortOrder(List<String> sortList, String sortDirection) {
        List<Sort.Order> sorts = new ArrayList<>();
        Sort.Direction direction;
        for (String sort : sortList) {
            if (sortDirection != null) {
                direction = Sort.Direction.fromString(sortDirection);
            } else {
                direction = Sort.Direction.DESC;
            }
            sorts.add(new Sort.Order(direction, sort));
        }
        return sorts;
    }

    @Override
    public Order updateOrder(Long id, Order order) {
        return repository.findById(id).map(
                orderNew -> {
                    orderNew.setUserId(order.getUserId());
                    orderNew.setProductId(order.getProductId());
                    orderNew.setQuantity(order.getQuantity());
                    orderNew.setCreateAt(order.getCreateAt());
                    orderNew.setStatus(order.getStatus());
                    return  repository.save(orderNew);
                }
        ).orElseThrow(()-> new NotFoundOrderException("Order not found with this id "+id));
    }

    @Override
    public void delete(Long id) {
         repository.deleteById(id);
    }

    @SneakyThrows
    @Override
    public PaymentObject getOrderToPay(Long id) {
       List<Order> orders =  repository.findAllByUserId(id).stream().filter(order -> order.getStatus().equals(false)).toList();
       PaymentObject paymentObject = new PaymentObject();
       paymentObject.setUserId(id);
       paymentObject.setProductCount(getCount(orders));
       paymentObject.setTotalPrice(orders.stream().mapToDouble(Order::getPrice).sum());
       paymentObject.setPayDate(new Date());
       paymentObject.setStatusId(1L);
       ProductObject productObject = new ProductObject();
       productObject.setUserId(id);
       productObject.setProductList(orders.stream().map(Order::getProductId).collect(Collectors.toList()));
       rabbitSender.sendMessage(new ObjectMapper().writeValueAsString(paymentObject));
       rabbitSender.sendListProduct(new ObjectMapper().writeValueAsString(productObject));
       return paymentObject;
    }

    @Override
    public List<Order> getOrderByUserId(Long id) {
        List<Order> orders = repository.findAllByUserId(id);
        if(orders.size()<1){
            throw new NotFoundUserException("Cant found user with id: "+ id);
        }
        return orders;
    }

    @Override
    public Order updateStatus(Long id) {
        return repository.findById(id).map(
                order -> {
                    order.setStatus(true);
                    return repository.save(order);

                }
        ).orElseThrow(()-> new NotFoundOrderException("Cant found Order with this id: "+id));
    }

    private Long getCount(List<Order> orders){
        Long result = 0L;
        for (int i = 0; i <orders.size() ; i++) {
            if(orders.get(i).getQuantity()>1){
                result+= orders.get(i).getQuantity()-1;
            }
        }
        return result+orders.stream().mapToLong(Order::getProductId).count();
    }


}
