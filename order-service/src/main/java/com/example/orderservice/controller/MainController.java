package com.example.orderservice.controller;

import com.example.orderservice.model.Order;
import com.example.orderservice.model.PaymentObject;
import com.example.orderservice.model.Response;
import com.example.orderservice.services.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = "/order")
public class MainController {

    private OrderService service;
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Response create(@RequestBody Order order){
        service.create(order);
        return Response.ok("successful");
    }

    @GetMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    public Response getAllOrders(@RequestParam(required = false) Long userId,
                                 @RequestParam(required = false) Long productId,
                                 @RequestParam(required = false) Integer quantity,
                                 @RequestParam(required = false) Double price,
                                 @RequestParam(required = false) Date createAt,
                                 @RequestParam(required = false) Boolean status,
                                 @RequestParam(defaultValue = "0") int page,
                                 @RequestParam(defaultValue = "10")int size,
                                 @RequestParam(defaultValue = "") List<String> sortList,
                                 @RequestParam(defaultValue = "DESC") String sort
                                 ){
        Page<Order> orderPage = service.getAllOrders(userId, productId, quantity, price, createAt, status, page, size, sortList, sort);
        return Response.ok(orderPage);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response getOrder(@PathVariable("id")Long id){
        return Response.ok(service.getOrder(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response updateOrder(@PathVariable("id") Long id, @RequestBody Order order){
        return Response.ok(service.updateOrder(id,order));
    }

    @GetMapping("/payments/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response getToPayForProduct(@PathVariable("id") Long id){
        PaymentObject paymentObject =  service.getOrderToPay(id);
        return Response.ok(paymentObject);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response deleteOrder(@PathVariable("id") Long id){
        return Response.ok("successful");
    }

}
