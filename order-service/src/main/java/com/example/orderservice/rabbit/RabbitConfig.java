package com.example.orderservice.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    Exchange orderPay() {
        return new TopicExchange("pay", true, false);
    }

    @Bean
    Queue orderPaymentObject() {
        return new Queue("payments", true, false, false);
    }

    @Bean
    Binding curtBinding(Queue orderPaymentObject, Exchange orderPay){
        return new Binding(orderPaymentObject.getName(), Binding.DestinationType.QUEUE, orderPay.getName(), "payments", null);
    }

    @Bean
    Queue infoPaymentObject() {
        return new Queue("info_payments", true, false, false);
    }

    @Bean
    Binding infoBinding(Queue infoPaymentObject, Exchange orderPay){
        return new Binding(infoPaymentObject.getName(), Binding.DestinationType.QUEUE, orderPay.getName(), "info_payments", null);
    }
}
