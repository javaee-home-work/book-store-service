package com.example.orderservice.rabbit;

import com.example.orderservice.model.Order;
import com.example.orderservice.model.PaymentObject;
import com.example.orderservice.services.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
public class RabbitListeners {

    private final OrderService service;
    @SneakyThrows
    @RabbitListener(queues = "addOrder")
    public void onOrderListener(String message){
        ObjectMapper objectMapper = new ObjectMapper();
        Order order = objectMapper.readValue(message,Order.class);
        service.create(order);
    }
    @SneakyThrows
    @RabbitListener(queues = "payments_result")
    public void updateAfterPay(String message){
        PaymentObject paymentObject = new ObjectMapper().readValue(message, PaymentObject.class);
        List<Order> orderList = service.getOrderByUserId(paymentObject.getUserId());
        log.info("order list {}", orderList);
        for (int i = 0; i <orderList.size() ; i++) {
            service.updateStatus(orderList.get(i).getId());
        }
    }
}
