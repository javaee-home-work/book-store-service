package com.example.orderservice.rabbit;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    public void sendMessage(String message){
        rabbitTemplate.convertAndSend("pay","payments", message);
    }

    public void sendListProduct(String message){
        rabbitTemplate.convertAndSend("pay","info_payments",message);
    }
}
