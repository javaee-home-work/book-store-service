package com.example.orderservice.services;

import com.example.orderservice.Exceptions.NotFoundOrderException;
import com.example.orderservice.Exceptions.NotFoundUserException;
import com.example.orderservice.model.Order;
import com.example.orderservice.model.PaymentObject;
import com.example.orderservice.model.ProductObject;
import com.example.orderservice.rabbit.RabbitSender;
import com.example.orderservice.repository.OrderRepository;
import com.example.orderservice.services.OrderServiceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.aspectj.weaver.ast.Or;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.predicate;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {
    @Mock
    private  OrderRepository orderRepository;
    @Mock
    private  RabbitSender rabbitSender;
    @InjectMocks
    private OrderServiceImpl orderService;

    private final Long ID = 1L;

    private final Long USER_ID = 1L;

    private final Long PRODUCT_ID = 2L;

    private final Integer QUANTITY = 2;

    private final Double PRICE = 150.0;


    @Test
    void create() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setPrice(PRICE);
        order.setQuantity(QUANTITY);

        when(orderRepository.save(order)).thenReturn(order);

        Order expected = orderService.create(order);
        assertThat(expected.getId()).isEqualTo(order.getId());
        assertThat(expected.getUserId()).isEqualTo(order.getUserId());
        assertThat(expected.getProductId()).isEqualTo(order.getProductId());
        assertThat(expected.getPrice()).isEqualTo(order.getPrice());
        assertThat(expected.getQuantity()).isEqualTo(order.getQuantity());


    }

    @Test
    void TestGetOrderByExistId() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setPrice(PRICE);
        order.setQuantity(QUANTITY);

        when(orderRepository.findById(ID)).thenReturn(Optional.of(order));

        Order expected = orderService.getOrder(ID);

        assertThat(expected.getId()).isEqualTo(order.getId());
        assertThat(expected.getUserId()).isEqualTo(order.getUserId());
        assertThat(expected.getProductId()).isEqualTo(order.getProductId());
        assertThat(expected.getPrice()).isEqualTo(order.getPrice());
        assertThat(expected.getQuantity()).isEqualTo(order.getQuantity());
    }

    @Test
    void TestGetOrderByNotExistId() {

        when(orderRepository.findById(ID)).thenThrow(NotFoundOrderException.class);

        assertThrows(NotFoundOrderException.class,()->{
            orderService.getOrder(ID);
        },"Cant found Order with this id: "+ID);
    }

    @Test
    void TestGetAllOrders() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setQuantity(QUANTITY);
        order.setStatus(false);

        Order orderNew = new Order();
        orderNew.setId(ID+1);
        orderNew.setUserId(USER_ID);
        orderNew.setProductId(PRODUCT_ID);
        orderNew.setQuantity(QUANTITY);
        orderNew.setStatus(false);

        Order orderThree = new Order();
        orderThree.setId(ID+2);
        orderThree.setUserId(USER_ID);
        orderThree.setProductId(PRODUCT_ID);
        orderThree.setQuantity(QUANTITY);
        orderThree.setStatus(false);
        int page = 0;
        int size = 10;
        List<String> sortList = Arrays.asList("userId", "productId");
        List<Order> orders = List.of(order,orderNew,orderThree);
        Page<Order> orderPage = new PageImpl<>(orders);
        String sortOrder = "DESC";

        when(orderRepository.findAll(any(Specification.class), Mockito.any(Pageable.class))).thenReturn(orderPage);

        Page<Order> expected = orderService.getAllOrders(USER_ID,PRODUCT_ID,QUANTITY,PRICE,null,false,page,size,sortList,sortOrder);

        assertThat(order.getProductId()).isEqualTo(expected.getContent().get(0).getProductId());
        assertThat(order.getUserId()).isEqualTo(expected.getContent().get(0).getUserId());
        assertThat(order.getQuantity()).isEqualTo(expected.getContent().get(0).getQuantity());
        assertThat(orderNew.getProductId()).isEqualTo(expected.getContent().get(1).getProductId());
        assertThat(orderNew.getUserId()).isEqualTo(expected.getContent().get(1).getUserId());
        assertThat(orderNew.getQuantity()).isEqualTo(expected.getContent().get(1).getQuantity());
        assertThat(orderThree.getProductId()).isEqualTo(expected.getContent().get(2).getProductId());
        assertThat(orderThree.getUserId()).isEqualTo(expected.getContent().get(2).getUserId());
        assertThat(orderThree.getQuantity()).isEqualTo(expected.getContent().get(2).getQuantity());

    }

    @Test
    void TestUpdateOrder() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setQuantity(QUANTITY);

        Order orderNew = new Order();
        orderNew.setId(ID);
        orderNew.setUserId(USER_ID+1);
        orderNew.setProductId(PRODUCT_ID+1);
        orderNew.setQuantity(QUANTITY+1);


        when(orderRepository.findById(ID)).thenReturn(Optional.of(order));
        when(orderRepository.save(orderNew)).thenReturn(orderNew);

        Order expected = orderService.updateOrder(ID,orderNew);

        assertThat(expected.getId()).isEqualTo(orderNew.getId());
        assertThat(expected.getUserId()).isEqualTo(orderNew.getUserId());
        assertThat(expected.getProductId()).isEqualTo(orderNew.getProductId());
        assertThat(expected.getQuantity()).isEqualTo(orderNew.getQuantity());
    }

    @Test
    void TestDeleteById() {
        doNothing().when(orderRepository).deleteById(ID);
        orderService.delete(ID);
        verify(orderRepository,times(1)).deleteById(ID);
    }
    @SneakyThrows
    @Test
    void TestGetOrderToPay() {
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID+3);
        order.setPrice(PRICE);
        order.setQuantity(QUANTITY);
        order.setStatus(false);

        Order orderNew = new Order();
        orderNew.setId(ID+1);
        orderNew.setUserId(USER_ID);
        orderNew.setProductId(PRODUCT_ID+1);
        orderNew.setPrice(PRICE);
        orderNew.setQuantity(QUANTITY+1);
        orderNew.setStatus(false);

        orders.add(order);
        orders.add(orderNew);
        when(orderRepository.findAllByUserId(USER_ID)).thenReturn(orders);

        PaymentObject result = orderService.getOrderToPay(USER_ID);

        assertNotNull(result);
        assertEquals(USER_ID, result.getUserId());
        ArgumentCaptor<String> paymentObjectCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> productObjectCaptor = ArgumentCaptor.forClass(String.class);
        verify(rabbitSender).sendMessage(paymentObjectCaptor.capture());
        verify(rabbitSender).sendListProduct(productObjectCaptor.capture());
        PaymentObject paymentObject = new ObjectMapper().readValue(paymentObjectCaptor.getValue(), PaymentObject.class);
        ProductObject productObject = new ObjectMapper().readValue(productObjectCaptor.getValue(), ProductObject.class);
        assertNotNull(productObject);
        assertEquals(USER_ID, productObject.getUserId());
        assertEquals(USER_ID, paymentObject.getUserId());
    }

    @Test
    void TestGetOrderByExistUserId() {
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID+3);
        order.setQuantity(QUANTITY);
        order.setStatus(true);

        Order orderNew = new Order();
        orderNew.setId(ID+1);
        orderNew.setUserId(USER_ID);
        orderNew.setProductId(PRODUCT_ID+1);
        orderNew.setQuantity(QUANTITY+1);
        orderNew.setStatus(true);
        orders.add(order);
        orders.add(orderNew);

        when(orderRepository.findAllByUserId(USER_ID)).thenReturn(orders);

        List<Order> expected = orderService.getOrderByUserId(USER_ID);
        assertEquals(expected.size(),orders.size());
        assertThat(expected.get(0).getUserId()).isEqualTo(orderNew.getUserId()).isEqualTo(order.getUserId());
        assertThat(expected.get(0).getProductId()).isEqualTo(orders.get(0).getProductId());
        assertThat(expected.get(1).getProductId()).isEqualTo(orders.get(1).getProductId());

    }

    @Test
    void TestGetOrderByNotExistUserId() {
        when(orderRepository.findAllByUserId(USER_ID)).thenThrow(NotFoundUserException.class);

        assertThrows(NotFoundUserException.class,()->{
            orderService.getOrderByUserId(USER_ID);
        },"Cant found user with id: "+USER_ID);

    }

    @Test
    void TestUpdateStatus() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setQuantity(QUANTITY);
        order.setPrice(PRICE);
        order.setStatus(false);

        when(orderRepository.findById(ID)).thenReturn(Optional.of(order));
        when(orderRepository.save(order)).thenReturn(order);

        Order expected = orderService.updateStatus(ID);
        assertThat(expected.getId()).isEqualTo(order.getId());
        assertThat(expected.getUserId()).isEqualTo(order.getUserId());
        assertThat(expected.getProductId()).isEqualTo(order.getProductId());
        assertThat(expected.getPrice()).isEqualTo(order.getPrice());
        assertThat(expected.getQuantity()).isEqualTo(order.getQuantity());
        assertThat(expected.getStatus()).isEqualTo(true);
    }
}