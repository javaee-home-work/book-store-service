package com.example.orderservice.controller;

import com.example.orderservice.Exceptions.NotFoundOrderException;
import com.example.orderservice.model.Order;
import com.example.orderservice.model.PaymentObject;
import com.example.orderservice.services.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.assertj.core.api.BDDAssumptions.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {MainControllerTest.class, ErrorController.class})
@AutoConfigureRestDocs
class MainControllerTest {
    @MockBean
    private OrderService service;

    private MockMvc mockMvc;

    private final Long ID = 1L;

    private final Long USER_ID = 1L;

    private final Long PRODUCT_ID = 2L;

    private final Integer QUANTITY = 2;

    private final Double PRICE = 150.0;

    @RegisterExtension
    final RestDocumentationExtension restDocumentation = new RestDocumentationExtension ();

    @BeforeEach
    public void setUp(RestDocumentationContextProvider restDocumentation) {
        MockitoAnnotations.initMocks(this);
        MainController mainController = new MainController(service);
        mockMvc = MockMvcBuilders.standaloneSetup(mainController)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }

    @SneakyThrows
    @Test
    void TestCreate() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setPrice(PRICE);
        order.setQuantity(QUANTITY);
        order.setCreateAt(new Date());
        order.setStatus(false);

        when(service.create(order)).thenReturn(order);

        mockMvc.perform(
                        post("/order/create")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(order)))
                .andDo(document("{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("id").description("Id of Order"),
                                fieldWithPath("userId").description("Id of user who add product to the basket"),
                                fieldWithPath("productId").description("Id of product"),
                                fieldWithPath("price").description("price of product"),
                                fieldWithPath("quantity").description("quantity of product"),
                                fieldWithPath("createAt").description("Date of create Order"),
                                fieldWithPath("status").description("Status Order: false(dont payment yet)/ true(already pay for order)")
                        )
                        ))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data").value("successful"));
    }

    @SneakyThrows
    @Test
    void TestGetAllOrders() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setQuantity(QUANTITY);
        order.setStatus(false);
        order.setCreateAt(new Date());
        PageImpl<Order> orderPage = new PageImpl<>(Collections.singletonList(order));

        when(service.getAllOrders(USER_ID, PRODUCT_ID, QUANTITY, PRICE, new Date(), false, 0, 5, Collections.emptyList(), "DESC")).thenReturn(orderPage);

        mockMvc.perform(get("/order/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(document("{class-name}/{method-name}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"));
    }

    @SneakyThrows
    @Test
    void TestGetOrderByExistId() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setQuantity(QUANTITY);
        order.setStatus(false);

        when(service.getOrder(ID)).thenReturn(order);

        mockMvc.perform(get("/order/{id}", ID))
                .andDo(document("{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("status").description("status of procedure: ok(its ok)/ fail(its fail)"),
                                fieldWithPath("data.id").description("Id of Order"),
                                fieldWithPath("data.userId").description("Id of user who add product to the basket"),
                                fieldWithPath("data.productId").description("Id of product"),
                                fieldWithPath("data.price").description("price of product"),
                                fieldWithPath("data.quantity").description("quantity of product"),
                                fieldWithPath("data.createAt").description("Date of create Order"),
                                fieldWithPath("data.status").description("Status Order: false(dont payment yet)/ true(already pay for order)")
                        )
                        ))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data.id").value(ID))
                .andExpect(jsonPath("$.data.userId").value(USER_ID))
                .andExpect(jsonPath("$.data.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.data.userId").value(USER_ID))
                .andExpect(jsonPath("$.data.quantity").value(QUANTITY))
                .andExpect(jsonPath("$.data.status").value(order.getStatus()));
    }

    @SneakyThrows
    @Test
    void TestGetOrderByNotExistId() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setQuantity(QUANTITY);
        order.setStatus(false);

        when(service.getOrder(ID)).thenThrow(NotFoundOrderException.class);

        mockMvc.perform(get("/order/{id}", ID))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @SneakyThrows
    @Test
    void TestUpdateOrder() {
        Order order = new Order();
        order.setId(ID);
        order.setUserId(USER_ID);
        order.setProductId(PRODUCT_ID);
        order.setQuantity(QUANTITY);

        Order orderNew = new Order();
        orderNew.setId(ID);
        orderNew.setUserId(USER_ID + 1);
        orderNew.setProductId(PRODUCT_ID + 1);
        orderNew.setQuantity(QUANTITY + 1);


        when(service.getOrder(ID)).thenReturn(order);
        when(service.create(orderNew)).thenReturn(orderNew);

        mockMvc.perform(put("/order/{id}", ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(orderNew)))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"));
    }

    @SneakyThrows
    @Test
    void TestGetToPayForProduct() {
        PaymentObject paymentObject = new PaymentObject();
        paymentObject.setId(ID);
        paymentObject.setUserId(USER_ID);
        paymentObject.setProductCount(4L);
        paymentObject.setTotalPrice(PRICE);
        paymentObject.setStatusId(1L);
        paymentObject.setPayDate(new Date());

        when(service.getOrderToPay(ID)).thenReturn(paymentObject);

        mockMvc.perform(get("/order/payments/{id}", ID))
                .andDo(document("{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("status").description("status of procedure: ok(its ok)/ fail(its fail)"),
                                fieldWithPath("data.id").description("Id of PaymentObject"),
                                fieldWithPath("data.userId").description("Id of user who add product to the basket"),
                                fieldWithPath("data.productCount").description("Count of all product by User ID"),
                                fieldWithPath("data.totalPrice").description("Total price of all product who be add to the basket/order"),
                                fieldWithPath("data.statusId").description("status ID: (1-created), (2-In progress), (3-Done)"),
                                fieldWithPath("data.payDate").description("Date to create a check payment")
                        )
                        ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data.id").value(ID))
                .andExpect(jsonPath("$.data.userId").value(USER_ID))
                .andExpect(jsonPath("$.data.productCount").value(paymentObject.getProductCount()))
                .andExpect(jsonPath("$.data.totalPrice").value(PRICE))
                .andExpect(jsonPath("$.data.statusId").value(paymentObject.getStatusId()));

    }

    @SneakyThrows
    @Test
    void TestDeleteOrder() {
        doNothing().when(service).delete(ID);

        mockMvc.perform(delete("/order/{id}",ID))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data").value("successful"));
    }
}