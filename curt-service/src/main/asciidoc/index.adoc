=== Curt Service

== Rest Api

*create Basket*`/curt/add`
include::{snippets}/basket-controller-test/test-create-basket-with-valid/curl-request.adoc[]
include::{snippets}/basket-controller-test/test-create-basket-with-valid/http-request.adoc[]
include::{snippets}/basket-controller-test/test-create-basket-with-valid/request-fields.adoc[]
include::{snippets}/basket-controller-test/test-create-basket-with-valid/response-body.adoc[]
include::{snippets}/basket-controller-test/test-create-basket-with-valid/http-response.adoc[]

*Get Basket By Id*`/curt/{id}`

include::{snippets}/basket-controller-test/test-get-basket-by-exist-id/curl-request.adoc[]
include::{snippets}/basket-controller-test/test-get-basket-by-exist-id/http-request.adoc[]
include::{snippets}/basket-controller-test/test-get-basket-by-exist-id/response-fields.adoc[]
include::{snippets}/basket-controller-test/test-get-basket-by-exist-id/response-body.adoc[]

*Get all basket By UserId*`/curt/user/{id}`

include::{snippets}/basket-controller-test/test-get-list-basket-by-user-id/curl-request.adoc[]
include::{snippets}/basket-controller-test/test-get-list-basket-by-user-id/http-request.adoc[]
include::{snippets}/basket-controller-test/test-get-list-basket-by-user-id/response-fields.adoc[]
include::{snippets}/basket-controller-test/test-get-list-basket-by-user-id/response-body.adoc[]
include::{snippets}/basket-controller-test/test-get-list-basket-by-user-id/http-response.adoc[]

*add Quantity Product in Basket*`/curt/{id}/addQuantity`

include::{snippets}/basket-controller-test/test-add-quantity/curl-request.adoc[]
include::{snippets}/basket-controller-test/test-add-quantity/http-request.adoc[]
include::{snippets}/basket-controller-test/test-add-quantity/response-body.adoc[]
*Minus quantity product in Basket*`/curt/{id}/minusQuantity`

include::{snippets}/basket-controller-test/test-minus-quantity/curl-request.adoc[]
include::{snippets}/basket-controller-test/test-minus-quantity/http-request.adoc[]
include::{snippets}/basket-controller-test/test-minus-quantity/response-body.adoc[]
*Delete Basket*`/curt/{id}`

include::{snippets}/basket-controller-test/test-delete-basket/curl-request.adoc[]
include::{snippets}/basket-controller-test/test-delete-basket/http-request.adoc[]
include::{snippets}/basket-controller-test/test-delete-basket/response-body.adoc[]
