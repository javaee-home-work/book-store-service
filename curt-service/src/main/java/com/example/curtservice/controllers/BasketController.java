package com.example.curtservice.controllers;

import com.example.curtservice.model.Basket;
import com.example.curtservice.model.Response;
import com.example.curtservice.services.BasketService;
import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping(value = "/curt", produces = MediaType.APPLICATION_JSON_VALUE)
public class BasketController {

    private BasketService service;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Response createBasket(@RequestBody @Valid Basket basket) throws JsonProcessingException {
        return Response.ok(service.create(basket));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response getBasketById(@PathVariable("id") Long id){
        return Response.ok(service.getBasketById(id));
    }

    @GetMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response getListBasketByUserId(@PathVariable("id") Long id){
        return Response.ok(service.getBasketListByUserId(id));
    }

    @PutMapping("/{id}/addQuantity")
    @ResponseStatus(HttpStatus.OK)
    public Response addQuantity(@PathVariable("id") Long id){
        return Response.ok(service.addQuantity(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response updateBasket(@PathVariable("id")Long id, Basket basket){
        return Response.ok(service.updateBasket(id,basket));
    }

    @PutMapping("/{id}/minusQuantity")
    @ResponseStatus(HttpStatus.OK)
    public Response minusQuantity(@PathVariable("id") Long id){
        return Response.ok(service.minusQuantity(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response deleteBasket(@PathVariable("id") Long id){
        service.deleteBasketById(id);
        return Response.ok("successful");
    }
}
