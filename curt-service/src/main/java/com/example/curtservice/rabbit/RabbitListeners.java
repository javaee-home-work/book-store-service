package com.example.curtservice.rabbit;

import com.example.curtservice.model.Basket;
import com.example.curtservice.model.PaymentObject;
import com.example.curtservice.services.BasketService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RabbitListeners {
    @Autowired
    private BasketService service;

    @SneakyThrows
    @RabbitListener(queues = "pay_result_to_basket")
    public void updateAfterPay(String message){
        PaymentObject paymentObject = new ObjectMapper().readValue(message, PaymentObject.class);
        List<Basket> basketList = service.getBasketListByUserId(paymentObject.getUserId());
        for (int i = 0; i <basketList.size() ; i++) {
            service.updateStatus(basketList.get(i).getId());
        }
    }
}
