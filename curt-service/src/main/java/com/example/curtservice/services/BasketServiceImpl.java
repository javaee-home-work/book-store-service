package com.example.curtservice.services;

import com.example.curtservice.model.Basket;
import com.example.curtservice.rabbit.RabbitSender;
import com.example.curtservice.repository.BasketRepository;
import com.example.curtservice.util.exeptions.DontHaveProductException;
import com.example.curtservice.util.exeptions.NotActiveUserExeption;
import com.example.curtservice.util.exeptions.NotFoundBookExeption;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class BasketServiceImpl implements BasketService {

    private final String URL_CATALOG = "lb://CATALOG-SERVICE/book/{id}";
    private final String URL_PROFILE = "lb://PROFILE-SERVICE/user/{id}";
     private RestTemplate restTemplate;
     private BasketRepository repository;
     private RabbitSender rabbitSender;



    @Override
    public Basket create(Basket basket) throws JsonProcessingException {
        if(!basket.getUserId().equals(checkProductAndUser(basket.getUserId(),URL_PROFILE))){
            throw new NotFoundBookExeption("User id is not available");
        }
        if(!basket.getProductId().equals(checkProductAndUser(basket.getProductId(),URL_CATALOG))){
            throw new NotFoundBookExeption("Product is not available");
        }
        if(!checkActiveUser(basket.getUserId())){
            throw new NotActiveUserExeption("User doesnt active");
        }
        Basket basketNew =  repository.save(basket);
        if(checkProductQuantity(basket.getProductId(), URL_CATALOG)==0 ){
            throw new DontHaveProductException("The product is end, quantity = 0");
        }
        if(checkProductQuantity(basket.getProductId(), URL_CATALOG)<basketNew.getQuantity()){
            throw new DontHaveProductException("Dont have "+ basketNew.getQuantity()+ " quantity for this product");
        }

        Basket basketEnd =  setPriceToTheBasket(basketNew.getId());
        if(basketEnd.getQuantity()>1){
          Double price =   basketEnd.getPrice()* basket.getQuantity();
          basketEnd.setPrice(price);
          repository.save(basketEnd);
        }
       rabbitSender.sendCurtInfoEvent(new ObjectMapper().writeValueAsString(basketEnd));
        return basketEnd;

    }

    public Integer checkProductQuantity(Long product_id, String url){
        ResponseEntity<Basket> responseEntity = restTemplate.getForEntity(
                url ,
                Basket.class,
                Map.of("id", product_id)
        );
        return responseEntity.getBody().getQuantity();
    }
    public Basket setPriceToTheBasket(Long id){
        return repository.findById(id).map(
                basket -> {
                    basket.setPrice(getPriceProduct(basket.getProductId()));
                    return repository.save(basket);
                }
        ).orElseThrow(()-> new NotFoundBookExeption("Product cant find with this id: " + id));
    }

    public Long checkProductAndUser(Long product_id, String url){
        ResponseEntity<Basket> responseEntity = restTemplate.getForEntity(
                url ,
                Basket.class,
                Map.of("id", product_id)
        );
        return responseEntity.getBody().getId();
    }


    public Boolean checkActiveUser(Long user_id) {
        ResponseEntity<Basket> responseEntity = restTemplate.getForEntity(
                URL_PROFILE,
                Basket.class,
                Map.of("id", user_id)
        );
        return responseEntity.getBody().getStatus();
    }

    public Double getPriceProduct(Long product_id){
        ResponseEntity<Basket> responseEntity = restTemplate.getForEntity(
                URL_CATALOG,
                Basket.class,
                Map.of("id", product_id)
        );
        return responseEntity.getBody().getPrice();
    }

    @Override
    public Basket getBasketById(Long id) {
        return repository.findById(id).orElseThrow(()-> new NotFoundBookExeption("Can't find basket with this id: " + id)) ;
    }

    @Override
    public List<Basket> getBasketListByUserId(Long user_id) {
        return repository.findAllByUserId(user_id);
    }

    @Override
    public Basket updateBasket(Long id, Basket basket) {
        return repository.findById(id).map(
                basket1 -> {
                    basket1.setQuantity(basket.getQuantity());
                    basket1.setProductId(basket.getProductId());
                    return repository.save(basket1);
                }
        ).orElseThrow(()-> new NotFoundBookExeption("Cant find basket with this id: " + id));
    }

    @Override
    public Basket addQuantity(Long id) {
    Integer basketOlQuantity = repository.findById(id).get().getQuantity();
        return repository.findById(id).map(
                basket -> {
                    basket.setQuantity(basketOlQuantity+1);
                    return  repository.save(basket);
                }
        ).orElseThrow(()-> new NotFoundBookExeption("Cant find basket with this id: " + id));

    }

    @Override
    public Basket minusQuantity(Long id) {
        Integer basketOlQuantity = repository.findById(id).get().getQuantity();
        return repository.findById(id).map(
                basket -> {
                    basket.setQuantity(basketOlQuantity-1);
                    return  repository.save(basket);
                }
        ).orElseThrow(()-> new NotFoundBookExeption("Cant find basket with this id: " + id));
    }

    @Override
    public void deleteBasketById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Basket updateStatus(Long id) {
        return repository.findById(id).map(
                basket -> {
                    basket.setStatus(true);
                    return repository.save(basket);
                }
        ).orElseThrow(()-> new NotFoundBookExeption("Basket not found with this id: "+ id));
    }
}
