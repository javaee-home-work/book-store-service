package com.example.curtservice.util.exeptions;

import com.example.curtservice.model.Response;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(DontHaveProductException.class)
    public Response dontHaveProduct(DontHaveProductException ex){
        return Response.fail().withMessage(ex.getMessage());
    }
    @ExceptionHandler(NotActiveUserExeption.class)
    public Response userNotActive(NotActiveUserExeption ex){
        return Response.fail().withMessage(ex.message);
    }

    @ExceptionHandler(NotFoundBookExeption.class)
    public Response NotFoundBook(NotFoundBookExeption ex){
        return Response.fail().withMessage(ex.message);
    }

    @ExceptionHandler(Exception.class)
    public Response handlerAllException(Exception exception){
        return Response.fail().withMessage(exception.getMessage());
    }
}
