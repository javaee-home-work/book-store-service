package com.example.curtservice.services;

import com.example.curtservice.model.Basket;
import com.example.curtservice.rabbit.RabbitSender;
import com.example.curtservice.repository.BasketRepository;
import com.example.curtservice.util.exeptions.NotFoundBookExeption;
import com.fasterxml.jackson.core.JsonProcessingException;

import static com.github.salomonbrys.kotson.BuilderKt.toJson;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.*;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;


import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;

import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import static org.mockito.MockitoAnnotations.initMocks;
@ExtendWith(MockitoExtension.class)
class BasketServiceImplTest {
    @Mock
    private RestTemplate restTemplate;

    @Mock
    private BasketRepository basketRepository;
    @Mock
    private RabbitSender rabbitSender;
    @Captor
    ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

    private final Long ID = 1L;

    private final Long USER_ID = 1L;

    private final Long PRODUCT_ID = 2L;

    private final Integer QUANTITY = 2;

    private final Double PRICE = 150.0;

    private final String URL_CATALOG = "lb://CATALOG-SERVICE/book/{id}";
    private final String URL_PROFILE = "lb://PROFILE-SERVICE/user/{id}";
    @InjectMocks
    private BasketServiceImpl basketService;
    @Before
    public void setUp(){
        initMocks(this);;
        this.basketService = new BasketServiceImpl(restTemplate,basketRepository,rabbitSender);
    }
    @SneakyThrows
    @Test
    public void TestTestCreateBasket() throws Exception {
        Basket basket = new Basket();
        basket.setId(ID);
        basket.setUserId(USER_ID);
        basket.setProductId(PRODUCT_ID);
        basket.setQuantity(QUANTITY);

        Basket profile = new Basket();
        profile.setId(USER_ID);
        profile.setStatus(true);
        when(restTemplate.getForEntity(URL_PROFILE,Basket.class, Map.of("id",USER_ID))).thenReturn(new ResponseEntity<>(profile, HttpStatus.OK));

        Basket catalog = new Basket();
        catalog.setId(PRODUCT_ID);
        catalog.setPrice(PRICE);
        catalog.setQuantity(QUANTITY);
        when(restTemplate.getForEntity(URL_CATALOG,Basket.class, Map.of("id", PRODUCT_ID))).thenReturn(new ResponseEntity<>(catalog, HttpStatus.OK));
        when(basketRepository.findById(ID)).thenReturn(Optional.of(basket));
        when(basketRepository.save(basket)).thenReturn(basket);

        Basket result = basketService.create(basket);
        verify(rabbitSender).sendCurtInfoEvent(messageCaptor.capture());
        String messageJson = messageCaptor.getValue();
        Basket basketRabbit = new ObjectMapper().readValue(messageJson, Basket.class);
        assertThat(basketRabbit.getUserId()).isEqualTo(basket.getUserId());
        assertThat(basketRabbit.getProductId()).isEqualTo(basket.getProductId());
        assertThat(basketRabbit.getPrice()).isEqualTo(basket.getPrice());
        assertNotNull(result);
        verify(basketRepository, times(3)).save(basket);
        verify(rabbitSender, times(1)).sendCurtInfoEvent(anyString());
    }

    @Test
    void TestTestCheckProductQuantity() {
        Basket basket = new Basket();
        basket.setUserId(USER_ID);
        basket.setProductId(PRODUCT_ID);
        basket.setQuantity(QUANTITY);
        basket.setPrice(PRICE);
        basket.setStatus(true);

        ResponseEntity<Basket> responseEntity = new ResponseEntity<>(basket,HttpStatus.OK);
        when(restTemplate.getForEntity(URL_CATALOG,Basket.class, Map.of("id", PRODUCT_ID))).thenReturn(responseEntity);

        Integer expected = basketService.checkProductQuantity(PRODUCT_ID ,URL_CATALOG);
        assertThat(expected).isEqualTo(responseEntity.getBody().getQuantity());
    }

    @Test
    void TestSetPriceToTheBasket() {
        Basket basket = new Basket();
        basket.setId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setPrice(PRICE);

        ResponseEntity<Basket> responseEntity = new ResponseEntity<>(basket, HttpStatus.OK);
        when(restTemplate.getForEntity(URL_CATALOG,Basket.class,Map.of("id", PRODUCT_ID))).thenReturn(responseEntity);

        when(basketRepository.findById(ID)).thenReturn(Optional.of(basket));
        when(basketRepository.save(basket)).thenReturn(basket);

        Basket expected = basketService.setPriceToTheBasket(ID);
        assertThat(expected.getPrice()).isEqualTo(responseEntity.getBody().getPrice());

    }

    @Test
    void TestCheckProductAndUser() {
        Basket profile = new Basket();
        profile.setId(USER_ID);
        when(restTemplate.getForEntity(URL_PROFILE,Basket.class, Map.of("id",USER_ID))).thenReturn(new ResponseEntity<>(profile, HttpStatus.OK));

        Long expected = basketService.checkProductAndUser(USER_ID, URL_PROFILE);
        assertThat(expected).isEqualTo(profile.getId());
    }

    @Test
    void TestCheckActiveUser() {
        Basket profile = new Basket();
        profile.setId(USER_ID);
        profile.setStatus(true);
        when(restTemplate.getForEntity(URL_PROFILE,Basket.class, Map.of("id",USER_ID))).thenReturn(new ResponseEntity<>(profile, HttpStatus.OK));

        Boolean expected = basketService.checkActiveUser(USER_ID);
        assertThat(expected).isEqualTo(profile.getStatus());

    }

    @Test
    void TestGetPriceProduct() {
        Basket basket = new Basket();
        basket.setId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setPrice(PRICE);

        ResponseEntity<Basket> responseEntity = new ResponseEntity<>(basket, HttpStatus.OK);
        when(restTemplate.getForEntity(URL_CATALOG,Basket.class,Map.of("id", PRODUCT_ID))).thenReturn(responseEntity);

        Double expected = basketService.getPriceProduct(PRODUCT_ID);
        assertThat(expected).isEqualTo(responseEntity.getBody().getPrice());
    }

    @Test
    void ValidTestGetBasketById() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);
        basket.setPrice(PRICE);

        when(basketRepository.findById(ID)).thenReturn(Optional.of(basket));

        Basket expected = basketService.getBasketById(ID);

        assertThat(expected.getId()).isEqualTo(basket.getId());
        assertThat(expected.getUserId()).isEqualTo(basket.getUserId());
        assertThat(expected.getProductId()).isEqualTo(basket.getProductId());
        assertThat(expected.getPrice()).isEqualTo(basket.getPrice());
        assertThat(expected.getQuantity()).isEqualTo(basket.getQuantity());

    }

    @Test
    void NotValidTestBasketGetNotExistId() {

        when(basketRepository.findById(ID)).thenThrow(NotFoundBookExeption.class);

        assertThrows(NotFoundBookExeption.class, ()->{
            basketService.getBasketById(ID);
        },"Cant find basket with this id: " + ID);
    }

    @Test
    void TestGetBasketListByUserId() {
        List<Basket> basketList = new ArrayList<>();
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);

        Basket basket2 = new Basket();
        basket2.setUserId(ID+1);
        basket2.setProductId(PRODUCT_ID+1);
        basket2.setUserId(USER_ID);
        basket2.setQuantity(QUANTITY+1);

        Basket basket3 = new Basket();
        basket3.setUserId(ID+2);
        basket3.setProductId(PRODUCT_ID+2);
        basket3.setUserId(USER_ID);
        basket3.setQuantity(QUANTITY+2);

        basketList.add(basket);
        basketList.add(basket2);
        basketList.add(basket3);

        when(basketRepository.findAllByUserId(USER_ID)).thenReturn(basketList);

        List<Basket> expected = basketService.getBasketListByUserId(USER_ID);

        assertThat(expected.size()).isEqualTo(3);
        assertThat(expected.get(0)).isSameAs(basket);
        assertThat(expected.get(1)).isSameAs(basket2);
        assertThat(expected.get(2)).isSameAs(basket3);
    }

    @Test
    void TestUpdateBasket() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);


        Basket basketNew = new Basket();
        basketNew.setUserId(ID);
        basketNew.setProductId(PRODUCT_ID+2);
        basketNew.setQuantity(QUANTITY+2);

        when(basketRepository.findById(ID)).thenReturn(Optional.of(basket));
        when(basketRepository.save(basketNew)).thenReturn(basketNew);

        Basket expected = basketService.updateBasket(ID,basketNew);

        assertThat(expected.getId()).isEqualTo(basketNew.getId());
        assertThat(expected.getUserId()).isEqualTo(basketNew.getUserId());
        assertThat(expected.getProductId()).isEqualTo(basketNew.getProductId());
        assertThat(expected.getPrice()).isEqualTo(basketNew.getPrice());
        assertThat(expected.getQuantity()).isEqualTo(basketNew.getQuantity());
    }

    @Test
    void TestAddQuantity() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);

        Basket basketNew = new Basket();
        basketNew.setUserId(ID);
        basketNew.setProductId(PRODUCT_ID);
        basketNew.setQuantity(QUANTITY+1);

        when(basketRepository.findById(ID)).thenReturn(Optional.of(basket));
        when(basketRepository.save(basketNew)).thenReturn(basketNew);

        Basket expected = basketService.addQuantity(ID);
        assertThat(expected.getId()).isEqualTo(basketNew.getId());
        assertThat(expected.getUserId()).isEqualTo(basketNew.getUserId());
        assertThat(expected.getProductId()).isEqualTo(basketNew.getProductId());
        assertThat(expected.getPrice()).isEqualTo(basketNew.getPrice());
        assertThat(expected.getQuantity()).isEqualTo(basketNew.getQuantity());
    }

    @Test
    void TestMinusQuantity() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setQuantity(QUANTITY);

        Basket basketNew = new Basket();
        basketNew.setUserId(ID);
        basketNew.setProductId(PRODUCT_ID);
        basketNew.setQuantity(QUANTITY-1);

        when(basketRepository.findById(ID)).thenReturn(Optional.of(basket));
        when(basketRepository.save(basketNew)).thenReturn(basketNew);

        Basket expected = basketService.minusQuantity(ID);
        assertThat(expected.getId()).isEqualTo(basketNew.getId());
        assertThat(expected.getProductId()).isEqualTo(basketNew.getProductId());
        assertThat(expected.getQuantity()).isEqualTo(basketNew.getQuantity());
    }

    @Test
    void TestDeleteBasketById() {
        doNothing().when(basketRepository).deleteById(ID);
        basketService.deleteBasketById(ID);
        verify(basketRepository, times(1)).deleteById(ID);
    }

    @Test
    void TestUpdateStatus() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);
        basket.setStatus(false);

        when(basketRepository.findById(ID)).thenReturn(Optional.of(basket));
        when(basketRepository.save(basket)).thenReturn(basket);

        Basket expected = basketService.updateStatus(ID);
        assertThat(expected.getId()).isEqualTo(basket.getId());
        assertThat(expected.getStatus()).isEqualTo(true);
        assertThat(expected.getProductId()).isEqualTo(basket.getProductId());
        assertThat(expected.getQuantity()).isEqualTo(basket.getQuantity());

    }
}