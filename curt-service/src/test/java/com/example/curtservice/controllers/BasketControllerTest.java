package com.example.curtservice.controllers;

import com.example.curtservice.model.Basket;
import com.example.curtservice.model.Response;
import com.example.curtservice.services.BasketService;
import com.example.curtservice.util.exeptions.DontHaveProductException;
import com.example.curtservice.util.exeptions.NotFoundBookExeption;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.swing.text.StyledEditorKit;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {BasketController.class, ErrorController.class})
class BasketControllerTest {
    @MockBean
    private BasketService service;

    private MockMvc mockMvc;

    private final Long ID = 1L;

    private final Long USER_ID = 1L;

    private final Long PRODUCT_ID = 2L;

    private final Integer QUANTITY = 2;

    private final Double PRICE = 150.0;

    private final Boolean STATUS = false;

    @RegisterExtension
    final RestDocumentationExtension restDocumentation = new RestDocumentationExtension();

    @BeforeEach
    public void setUp(RestDocumentationContextProvider restDocumentation) {
        MockitoAnnotations.initMocks(this);
        BasketController mainController = new BasketController(service);
        mockMvc = MockMvcBuilders.standaloneSetup(mainController)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }

    @Test
    void TestCreateBasketWithValid() throws Exception {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);
        basket.setPrice(PRICE);
        basket.setStatus(STATUS);

        when(service.create(basket)).thenReturn(basket);

        mockMvc.perform(
                        post("/curt/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(basket)))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("id").description("Id of Basket"),
                                fieldWithPath("userId").description("Id of user who add product to the basket"),
                                fieldWithPath("productId").description("Id of product"),
                                fieldWithPath("price").description("price of product"),
                                fieldWithPath("quantity").description("quantity of product"),
                                fieldWithPath("status").description("Status Order: false(dont payment yet)/ true(already pay for basket)")
                        )
                ))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data.userId").value(ID))
                .andExpect(jsonPath("$.data.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.data.quantity").value(QUANTITY))
                .andExpect(jsonPath("$.data.price").value(PRICE))
                .andExpect(jsonPath("$.data.status").value(STATUS));
    }

    @Test
    void TestCreateBasketWithINValid() throws Exception {
        Basket basket = new Basket();

        when(service.create(basket)).thenThrow(DontHaveProductException.class);

        mockMvc.perform(
                        post("/curt/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(basket)))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void TestGetBasketByExistId() throws Exception {
        Basket basket = new Basket();
        basket.setId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);
        basket.setPrice(PRICE);
        basket.setStatus(STATUS);

        when(service.getBasketById(ID)).thenReturn(basket);

        mockMvc.perform(get("/curt/{id}", ID))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("status").description("status of procedure: ok(its ok)/ fail(its fail)"),
                                fieldWithPath("data.id").description("Id of Basket"),
                                fieldWithPath("data.userId").description("Id of user who add product to the basket"),
                                fieldWithPath("data.productId").description("Id of product"),
                                fieldWithPath("data.price").description("price of product"),
                                fieldWithPath("data.quantity").description("quantity of product"),
                                fieldWithPath("data.status").description("Status Order: false(dont payment yet)/ true(already pay for basket)")
                        )
                ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value(ID))
                .andExpect(jsonPath("$.data.userId").value(USER_ID))
                .andExpect(jsonPath("$.data.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.data.quantity").value(QUANTITY))
                .andExpect(jsonPath("$.data.price").value(PRICE))
                .andExpect(jsonPath("$.data.status").value(STATUS));

    }

    @Test
    void TestGetBasketByNotExistId() throws Exception {

        when(service.getBasketById(ID)).thenThrow(NotFoundBookExeption.class);

        mockMvc.perform(get("/curt/{id}", ID))
                .andDo(document("{class-name}/{method-name}"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void TestGetListBasketByUserId() throws Exception {
        List<Basket> basketList = new ArrayList<>();
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);

        Basket basket2 = new Basket();
        basket2.setUserId(ID + 1);
        basket2.setProductId(PRODUCT_ID + 1);
        basket2.setUserId(USER_ID);
        basket2.setQuantity(QUANTITY + 1);

        Basket basket3 = new Basket();
        basket3.setUserId(ID + 2);
        basket3.setProductId(PRODUCT_ID + 2);
        basket3.setUserId(USER_ID);
        basket3.setQuantity(QUANTITY + 2);

        basketList.add(basket);
        basketList.add(basket2);
        basketList.add(basket3);

        when(service.getBasketListByUserId(USER_ID)).thenReturn(basketList);

        mockMvc.perform(get("/curt/user/{id}", ID))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("status").description("status of procedure: ok(its ok)/ fail(its fail)"),
                                fieldWithPath("data[0].id").description("Id of Basket"),
                                fieldWithPath("data[0].userId").description("Id of user who add product to the basket"),
                                fieldWithPath("data[0].productId").description("Id of product"),
                                fieldWithPath("data[0].price").description("price of product"),
                                fieldWithPath("data[0].quantity").description("quantity of product"),
                                fieldWithPath("data[0].status").description("Status Order: false(dont payment yet)/ true(already pay for basket)")
                        )
                ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data[0].id").value(basket.getId()))
                .andExpect(jsonPath("$.data[0].userId").value(basket.getUserId()))
                .andExpect(jsonPath("$.data[0].productId").value(basket.getProductId()))
                .andExpect(jsonPath("$.data[0].quantity").value(basket.getQuantity()))
                .andExpect(jsonPath("$.data[1].id").value(basket2.getId()))
                .andExpect(jsonPath("$.data[1].userId").value(basket2.getUserId()))
                .andExpect(jsonPath("$.data[1].productId").value(basket2.getProductId()))
                .andExpect(jsonPath("$.data[1].quantity").value(basket2.getQuantity()))
                .andExpect(jsonPath("$.data[2].id").value(basket3.getId()))
                .andExpect(jsonPath("$.data[2].userId").value(basket3.getUserId()))
                .andExpect(jsonPath("$.data[2].productId").value(basket3.getProductId()))
                .andExpect(jsonPath("$.data[2].quantity").value(basket3.getQuantity()));
    }

    @SneakyThrows
    @Test
    void TestAddQuantity() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);

        Basket basketNew = new Basket();
        basketNew.setUserId(ID);
        basketNew.setProductId(PRODUCT_ID);
        basketNew.setQuantity(QUANTITY + 1);

        when(service.getBasketById(ID)).thenReturn(basket);
        when(service.create(basketNew)).thenReturn(basketNew);

        mockMvc.perform(put("/curt/{id}/addQuantity", ID))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"));

    }

    @SneakyThrows
    @Test
    void TestUpdateBasket() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);


        Basket basketNew = new Basket();
        basketNew.setUserId(ID);
        basketNew.setProductId(PRODUCT_ID + 2);
        basketNew.setQuantity(QUANTITY + 2);

        when(service.getBasketById(ID)).thenReturn(basket);
        when(service.create(basketNew)).thenReturn(basketNew);

        mockMvc.perform(put("/curt/{id}", ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .contentType(new ObjectMapper().writeValueAsString(basketNew)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"));
    }

    @SneakyThrows
    @Test
    void TestMinusQuantity() {
        Basket basket = new Basket();
        basket.setUserId(ID);
        basket.setProductId(PRODUCT_ID);
        basket.setUserId(USER_ID);
        basket.setQuantity(QUANTITY);

        Basket basketNew = new Basket();
        basketNew.setUserId(ID);
        basketNew.setProductId(PRODUCT_ID);
        basketNew.setQuantity(QUANTITY - 1);

        when(service.getBasketById(ID)).thenReturn(basket);
        when(service.create(basketNew)).thenReturn(basketNew);

        mockMvc.perform(put("/curt/{id}/minusQuantity", ID))
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"));
    }

    @SneakyThrows
    @Test
    void TestDeleteBasket() {
        doNothing().when(service).deleteBasketById(ID);
        mockMvc.perform(delete("/curt/{id}", ID))
                .andDo(print())
                .andDo(document("{class-name}/{method-name}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.data").value("successful"));
        verify(service, times(1)).deleteBasketById(ID);

    }
}